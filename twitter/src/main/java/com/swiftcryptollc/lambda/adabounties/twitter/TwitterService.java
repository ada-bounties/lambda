package com.swiftcryptollc.lambda.adabounties.twitter;

import ch.qos.logback.classic.Level;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import com.swiftcryptollc.commons.adabounties.data.BountyStatus;
import com.swiftcryptollc.commons.adabounties.data.SearchData;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.data.Alias;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.Leaderboard;
import com.swiftcryptollc.commons.adabounties.es.data.TwitterUpdate;
import com.swiftcryptollc.commons.adabounties.es.query.AliasQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.LeaderboardQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.TwitterUpdateQueryHandler;
import com.swiftcryptollc.commons.adabounties.integrations.twitter.TwitterHandler;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

/**
 * Run as a "cron job" that is triggered from a event bridge event
 *
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public final class TwitterService implements RequestHandler<Object, String> {

    private final ObjectMapper mapper = new ObjectMapper();
    private final static LambdaElasticsearchClientConfig esClientConfig;
    private final static ElasticsearchClient esClient;
    private final static LeaderboardQueryHandler leaderQueryHandler;
    private final static BountyQueryHandler bountyQueryHandler;
    private final static TwitterUpdateQueryHandler twitterQueryHandler;
    private final static TwitterHandler twitterHandler;
    private final static AliasQueryHandler aliasQueryHandler;
    private final static Object LOCK = new Object();
    protected UUID id;
    protected final Boolean send;
    private final static Long ONE_DAY_AGO = 86400000L;
    private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd");

    protected final static Logger logger = LoggerFactory.getLogger(TwitterService.class);

    static {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        esClientConfig = new LambdaElasticsearchClientConfig();
        esClient = esClientConfig.elasticSearchClient();
        bountyQueryHandler = new BountyQueryHandler();
        bountyQueryHandler.setClient(esClient);
        leaderQueryHandler = new LeaderboardQueryHandler();
        leaderQueryHandler.setClient(esClient);
        twitterQueryHandler = new TwitterUpdateQueryHandler();
        twitterQueryHandler.setClient(esClient);
        aliasQueryHandler = new AliasQueryHandler();
        aliasQueryHandler.setClient(esClient);
        twitterHandler = new TwitterHandler();
        twitterHandler.updateSecrets(SecretsManager.getSecretMap());
    }

    /**
     * Default Constructor
     */
    public TwitterService() {
        send = Boolean.TRUE;
    }

    public TwitterService(Boolean send) {
        this.send = send;
    }

    /**
     * We aren't using the event information since this is a simple time
     * scheduled "cron" job
     *
     * Max Lambda execution time is 15 minutes.
     *
     * @param eventObject
     * @param context
     * @return
     */
    @Override
    public String handleRequest(Object eventObject, Context context) {
        Date start = new Date();
        Long nowMs = start.getTime();
        List<TwitterUpdate> tweets = new ArrayList<>();
        logger.info("Generating a bounty tweet");
        Set<TwitterUpdate> twitterUpdates = twitterQueryHandler.searchTweet(null, null, null, (nowMs - ONE_DAY_AGO));
        sendBountyTweet(twitterUpdates, nowMs, tweets);
        // sendLeaderboardTweet(twitterUpdates, start, tweets);
        logger.info("Deleting old twitter updates");
        twitterQueryHandler.deleteTwitterUpdates(nowMs);
        logger.info("Saving new twitter updates");
        saveTweets(tweets);
        Date end = new Date();
        Long timeSec = (end.getTime() - start.getTime()) / 1000l;
        logger.info("FInished! Processed in [" + timeSec + "] seconds");

        return "Ok";
    }

    /**
     *
     * @param twitterUpdates
     * @param nowMs
     * @param tweets
     */
    public void sendBountyTweet(Set<TwitterUpdate> twitterUpdates, Long nowMs, List<TwitterUpdate> tweets) {
        SearchData searchData = new SearchData();
        searchData.setStatus(BountyStatus.OPEN.label);
        PageRequest pageable = PageRequest.of(1, 50);
        Set<Bounty> bounties = bountyQueryHandler.searchBounties(searchData, pageable);
        Map<String, Bounty> bountyMap = new HashMap<>();

        bounties.forEach(bounty -> {
            bountyMap.put(bounty.getId(), bounty);
        });

        // Remove any bounties that we tweeted about in the last 24 hours
        for (TwitterUpdate update : twitterUpdates) {
            if (update.getBountyId() != null) {
                bountyMap.remove(update.getBountyId());
            }
        }
        if (!bountyMap.isEmpty()) {
            Bounty[] bountyArray = bountyMap.values().toArray(new Bounty[0]);
            SecureRandom sr = new SecureRandom();
            Integer i = sr.nextInt(0, bountyArray.length);
            Bounty bounty = bountyArray[i];
            logger.info("Sending Bounty tweet for [" + bounty.getId() + "]");
            Long tweetId = twitterHandler.sendOpenBountyTweet(bounty, send);
            if (tweetId != null) {
                TwitterUpdate tUpdate = new TwitterUpdate();
                tUpdate.setId(tweetId.toString());
                tUpdate.setBountyId(bounty.getId());
                tUpdate.setUpdateTimeMs(nowMs);
                tweets.add(tUpdate);
            }
        }

    }

    /**
     *
     * @param twitterUpdates
     * @param now
     * @param tweets
     */
    public void sendLeaderboardTweet(Set<TwitterUpdate> twitterUpdates, Date now, List<TwitterUpdate> tweets) {
        String ymd = SDF.format(now);

        List<String> leaderboards = new ArrayList<>(Arrays.asList("rep:Reputation",
                "btycmt:Bounty comments",
                "btyup:Bounty up votes",
                "btydwn:Bounty down votes",
                "cmtup:Comment up votes",
                "cmtdwn:Comment down votes",
                "replyto:Comment replies received",
                "claimson:Bounty claims received",
                "openbty:Open bounties",
                "clmdbty:Claimed bounties",
                "expbty:Expired bounties",
                "stalebty:Stale bounties",
                "totbty:Total bounties created",
                "openclm:Open claims",
                "accclm:Accepted claims",
                "rejclm:Rejected claims",
                "totclm:Total claims made",
                "tbtada:Bounty ADA paid",
                "tctada:Bounty ADA claimed"));

        Map<String, String> lbToTitle = new HashMap<>();
        leaderboards.forEach(lbString -> {
            String[] split = lbString.split(":");
            lbToTitle.put(split[0], split[1]);
        });

        for (TwitterUpdate update : twitterUpdates) {
            String lb = update.getLeaderboard();
            if (lb != null) {
                for (int i = 0; i < leaderboards.size(); ++i) {
                    if (leaderboards.get(i).equals(lb)) {
                        leaderboards.remove(i);
                        break;
                    }
                }
            }
        }

        boolean hasThree = false;
        Leaderboard lb = null;
        int i = 0;
        Boolean sent = false;
        do {
            do {
                SecureRandom sr = new SecureRandom();
                i = sr.nextInt(0, leaderboards.size());
                String[] split = leaderboards.get(i).split(":");
                lb = leaderQueryHandler.getLeaderboard(split[0], ymd);
                if (lb != null) {
                    logger.info("Checking leaderboard [" + lb.getType() + "]");
                }
                if ((lb != null) && (lb.getEntries().size() >= 3)) {
                    hasThree = true;
                } else {
                    leaderboards.remove(i);
                }
            } while ((!hasThree) && (!leaderboards.isEmpty()));

            if ((hasThree) && (lb != null)) {
                String[] names = new String[3];
                Double[] values = new Double[3];
                lb.getEntries().forEach(le -> {
                    if (null != le.getRank()) {
                        switch (le.getRank()) {
                            case 1: {
                                Alias alias = aliasQueryHandler.getAlias(le.getAccountId());
                                if (alias != null) {
                                    names[0] = alias.getName();
                                }
                                values[0] = le.getValue();
                                break;
                            }
                            case 2: {
                                Alias alias = aliasQueryHandler.getAlias(le.getAccountId());
                                if (alias != null) {
                                    names[1] = alias.getName();
                                }
                                values[1] = le.getValue();
                                break;
                            }
                            case 3: {
                                Alias alias = aliasQueryHandler.getAlias(le.getAccountId());
                                if (alias != null) {
                                    names[2] = alias.getName();
                                }
                                values[2] = le.getValue();
                                break;
                            }
                            default:
                                break;
                        }
                    }
                });
                boolean valid = true;
                for (String name : names) {
                    if ((name == null) || (name.isBlank())) {
                        valid = false;
                    }
                }
                if (valid) {
                    String keyValue = lb.getType().concat(":").concat(lbToTitle.get(lb.getType()));
                    logger.info("Sending Top 3 Tweet for [" + lb.getType() + "]");
                    Long tweetId = twitterHandler.sendTopThree(lbToTitle.get(lb.getType()), names, values, send);
                    if (tweetId != null) {
                        TwitterUpdate tUpdate = new TwitterUpdate();
                        tUpdate.setId(tweetId.toString());
                        tUpdate.setLeaderboard(keyValue);
                        tUpdate.setUpdateTimeMs(now.getTime());
                        tweets.add(tUpdate);
                        sent = true;
                    }
                }
            }
            try {
                leaderboards.remove(i);
            } catch (Exception e) {
            }
        } while ((!sent) && (!leaderboards.isEmpty()));
    }

    public void saveTweets(List<TwitterUpdate> tweets) {
        try {
            if (!tweets.isEmpty()) {
                BulkRequest.Builder brLb = new BulkRequest.Builder();
                for (TwitterUpdate tweet : tweets) {
                    ObjectNode node = mapper.convertValue(tweet, ObjectNode.class);
                    node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.TwitterUpdate");
                    brLb.operations(op -> op
                            .index(idx -> idx
                            .index(TwitterUpdate.INDEX_NAME)
                            .id(tweet.getId())
                            .document(node)
                            )
                    );
                }
                BulkResponse result = esClient.bulk(brLb.build());
                if (result.errors()) {
                    logger.error("Bulk save errors encountered");
                    for (BulkResponseItem item : result.items()) {
                        if (item.error() != null) {
                            logger.error(item.error().reason());
                        }
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("Exception occured while updating tweets! [" + ex.getMessage() + "]", ex);
        }
    }
}
