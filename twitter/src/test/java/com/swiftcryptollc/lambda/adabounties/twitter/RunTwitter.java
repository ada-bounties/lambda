package com.swiftcryptollc.lambda.adabounties.twitter;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class RunTwitter {

    public static void main(String[] args) {
        TwitterService ls = new TwitterService(false);
        ls.handleRequest(null, null);
    }
}
