package com.swiftcryptollc.lambda.adabounties.notifications;

import com.swiftcryptollc.commons.adabounties.aws.email.data.NotificationSummary;
import com.swiftcryptollc.commons.adabounties.es.query.NotificationQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class NotificationsAccountThread implements Runnable {

    protected final String id;
    protected final String email;
    private final Long last24 = 86400000L;
    private final NotificationsService notificationService;
    private final String accountId;
    private final String challenge;
    private final NotificationQueryHandler queryHandler;
    protected final static Logger logger = LoggerFactory.getLogger(NotificationsAccountThread.class);

    /**
     *
     * @param notificationService
     * @param accountId
     * @param email
     * @param queryHandler
     */
    public NotificationsAccountThread(NotificationsService notificationService, String accountId, String email, NotificationQueryHandler queryHandler, String challenge) {
        this.notificationService = notificationService;
        this.accountId = accountId;
        this.email = email;
        this.queryHandler = queryHandler;
        this.challenge = challenge;
        this.id = Base64Id.getNewId();
    }

    /**
     *
     */
    @Override
    public void run() {
        Long nowMs = new Date().getTime();
        try {
            Long startTimeMs = nowMs - last24;
            logger.info("Account [" + accountId + "] from [" + startTimeMs + "]");
            NotificationSummary notSummary = new NotificationSummary();
            notSummary.setEmail(email);
            queryHandler.getStatusCountForNotificationsByAccount(accountId, startTimeMs, notSummary);
            // Setup email
            notificationService.finishedAccountThread(id, notSummary, challenge);
        } catch (Exception ex) {
            logger.error("Exception occured while processing account [" + accountId + "] [" + ex.getMessage() + "]");
        }

        logger.info("Finished account [" + accountId + "]");
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
}
