package com.swiftcryptollc.lambda.adabounties.notifications;

import ch.qos.logback.classic.Level;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.swiftcryptollc.commons.adabounties.aws.email.EmailUtils;
import com.swiftcryptollc.commons.adabounties.aws.email.data.NotificationSummary;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.query.NotificationQueryHandler;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Run as a "cron job" that is triggered from a new file in an S3 bucket
 *
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class NotificationsService implements RequestHandler<S3Event, String> {

    private final static EmailUtils emailUtils;
    private final Set<String> threadIds = new HashSet<>();
    private final static LambdaElasticsearchClientConfig esClientConfig;
    private final static ElasticsearchClient esClient;
    private final static NotificationQueryHandler notificationQueryHandler;
    protected final static Logger logger = LoggerFactory.getLogger(NotificationsService.class);

    static {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        esClientConfig = new LambdaElasticsearchClientConfig();
        esClient = esClientConfig.elasticSearchClient();
        notificationQueryHandler = new NotificationQueryHandler(esClient);
        emailUtils = new EmailUtils();
    }

    /**
     * Default Constructor
     */
    public NotificationsService() {
    }

    /**
     * We aren't using the event information since this is a simple time
     * scheduled "cron" job
     *
     * Max Lambda execution time is 15 minutes.
     *
     * @param s3event
     * @param context
     * @return
     */
    @Override
    public String handleRequest(S3Event s3event, Context context) {
        Date start = new Date();
        if ((s3event.getRecords() != null) && (s3event.getRecords().size() > 0)) {
            S3EventNotification.S3EventNotificationRecord record = s3event.getRecords().get(0);
            String srcBucket = record.getS3().getBucket().getName();
            String srcKey = record.getS3().getObject().getUrlDecodedKey();
            logger.info("Processing S3 Object [" + srcBucket + "] [" + srcKey + "]");
            AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
            S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcBucket, srcKey));
            InputStream objectData = s3Object.getObjectContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(objectData));
            String line = "";
            int i = 0;
            try {
                while ((line = br.readLine()) != null) {
                    i = processLine(line, i);
                }
            } catch (Exception ex) {
                logger.error("Exception occured reading the S3 file! [" + ex.getMessage() + "]");
            }
            // Remove the processed file
            s3Client.deleteObject(srcBucket, srcKey);
            int sleepCtr = 0;
            while (!threadIds.isEmpty()) {
                try {
                    Thread.sleep(500);
                    if (++sleepCtr > 60) {
                        logger.info("Waiting for [" + threadIds.size() + "] account threads to finish");
                    }
                } catch (Exception e) {
                }
            }
            Date end = new Date();
            Long timeSec = (end.getTime() - start.getTime()) / 1000l;
            logger.info("Processed [" + i + "] accounts in [" + timeSec + "] seconds");

        }
        return "Ok";
    }

    /**
     *
     * @param line
     * @param i
     * @return
     */
    public int processLine(String line, int i) {
        line = line.trim();
        if ((!line.isEmpty()) && (line.contains("="))) {
            ++i;
            String[] split = line.split("=");
            String accountId = split[0];
            String email = split[1];
            String challenge = split[2];
            logger.info("Starting thread for [" + accountId + "]");
            NotificationsAccountThread accountThread = new NotificationsAccountThread(this, accountId, email, notificationQueryHandler, challenge);
            threadIds.add(accountThread.getId());
            Thread thread = new Thread(accountThread);
            thread.start();
        }

        return i;

    }

    /**
     * As each thread finishes, it'll call this method
     *
     * @param id
     * @param notSum
     * @param challenge
     */
    public void finishedAccountThread(String id, NotificationSummary notSum, String challenge) {
        if (notSum.getNumNotifications() > 0) {
            emailUtils.sendSummaryNotification(notSum, challenge);
        }
        threadIds.remove(id);
    }

    /**
     *
     * @return
     */
    public int getThreadIdsSize() {
        return threadIds.size();
    }

}
