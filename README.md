# Lambda

These lambda modules are used within AWS Lambda. Most are designed to function like "cron" jobs asynchronously from the ADA Bounties servers.

## Functions and layouts

<b>User Metrics Kickstarter</b>
<ul>
<li><i class="fas fa-caret-right"></i><li>Pulls all accounts and writes account metadata to S3 files to allow for user metrics parallel processing.</li>
</ul>

<b>User Metrics</b>
<ul>
<li><i class="fas fa-caret-right"></i><li>Creates metrics for each user based on the last 30, 60, and 90 days as well as all time.</li>
<li><i class="fas fa-caret-right"></i><li>Metrics include number of open, closed, claimed, expired, and stale bounties a user has. It includes the number of open, claimed, accepted, and rejected claims. It also includes the number of bounty and claim comments.</li>
</ul>

<b>User Notifications Kickstarter</b>
<ul>
<li><i class="fas fa-caret-right"></i><li>Pulls all accounts and if the user elected for email notifications, writes their account metadata to S3 files to allow for user notifications parallel processing.</li>
</ul>

<b>User Notifications</b>
<ul>
<li><i class="fas fa-caret-right"></i><li>Generates a summary of the different notification types for each user. Notification types include bookmark, bounty, and claim updates, claim submissions to a user's bounty, comment replies, and more.</li>
</ul>

<b>Expired Bounty Service</b>
<ul>
<li><i class="fas fa-caret-right"></i><li>Checks all open bounties against their expiration date/time. If the current time is after the expiration time, the bounty will be marked and saved as expired.</li>
</ul>

<b>Stale Bounty Service</b>
<ul>
<li><i class="fas fa-caret-right"></i><li>Checks all open bounties against their "last interacted" (last time someone commented, reacted, or did anything with the particular bounty) date/time. If the current time is after the last interacted time, the bounty will be marked and saved as expired.</li>
</ul>

