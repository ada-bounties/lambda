package com.swiftcryptollc.lambda.adabounties.metrics;

import ch.qos.logback.classic.Level;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;

import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.data.TokenBountyTotal;
import com.swiftcryptollc.commons.adabounties.es.data.TokenClaimTotal;
import com.swiftcryptollc.commons.adabounties.es.data.UserMetric;
import com.swiftcryptollc.commons.adabounties.es.query.AccountingQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.ClaimQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.CommentQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.timers.TimeoutInterface;
import com.swiftcryptollc.commons.adabounties.utilities.timers.TimeoutTask;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Run as a "cron job" that is triggered from a new file in an S3 bucket
 *
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public final class UserMetricsService implements RequestHandler<S3Event, String>, TimeoutInterface {

    private final ObjectMapper mapper = new ObjectMapper();
    private final Set<String> threadIds = new HashSet<>();
    private final Set<UserMetric> userMetrics = new HashSet<>();
    private final Set<TokenBountyTotal> tbts = new HashSet<>();
    private final Set<TokenClaimTotal> tcts = new HashSet<>();
    private final static LambdaElasticsearchClientConfig esClientConfig;
    private final static ElasticsearchClient esClient;
    private final static AccountingQueryHandler accountingQueryHandler;
    private final static BountyQueryHandler bountyQueryHandler;
    private final static ClaimQueryHandler claimQueryHandler;
    private final static CommentQueryHandler commentQueryHandler;
    private final static Object LOCK = new Object();
    private final static Object LOCK_TBT = new Object();
    private final static Object LOCK_TCT = new Object();
    protected UUID id;
    protected final static Logger logger = LoggerFactory.getLogger(UserMetricsService.class);

    static {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        esClientConfig = new LambdaElasticsearchClientConfig();
        esClient = esClientConfig.elasticSearchClient();
        bountyQueryHandler = new BountyQueryHandler();
        bountyQueryHandler.setClient(esClient);
        claimQueryHandler = new ClaimQueryHandler();
        claimQueryHandler.setClient(esClient);
        commentQueryHandler = new CommentQueryHandler();
        commentQueryHandler.setClient(esClient);
        accountingQueryHandler = new AccountingQueryHandler();
        accountingQueryHandler.setClient(esClient);
    }

    /**
     * Default Constructor
     */
    public UserMetricsService() {
    }

    /**
     * We aren't using the event information since this is a simple time
     * scheduled "cron" job
     *
     * Max Lambda execution time is 15 minutes.
     *
     * @param s3event
     * @param context
     * @return
     */
    @Override
    public String handleRequest(S3Event s3event, Context context) {
        Date start = new Date();
        if ((s3event.getRecords() != null) && (s3event.getRecords().size() > 0)) {
            S3EventNotification.S3EventNotificationRecord record = s3event.getRecords().get(0);
            String srcBucket = record.getS3().getBucket().getName();
            String srcKey = record.getS3().getObject().getUrlDecodedKey();
            logger.info("Processing S3 Object [" + srcBucket + "] [" + srcKey + "]");
            AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
            S3Object s3Object = s3Client.getObject(new GetObjectRequest(srcBucket, srcKey));
            InputStream objectData = s3Object.getObjectContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(objectData));
            String accountId = "";
            int i = 0;
            // Timeout task to run bulk inserts
            TimeoutTask timeoutTask = new TimeoutTask(this);
            Timer timer = new Timer();
            timer.schedule(timeoutTask, 15000, 15000);
            try {
                while ((accountId = br.readLine()) != null) {
                    i = processLine(accountId, i);
                }
            } catch (Exception ex) {
                logger.error("Exception occured reading the S3 file! [" + ex.getMessage() + "]");
            }
            // Remove the processed file
            s3Client.deleteObject(srcBucket, srcKey);
            int sleepCtr = 0;
            while (!threadIds.isEmpty()) {
                try {
                    Thread.sleep(500);
                    if (++sleepCtr > 60) {
                        logger.info("Waiting for [" + threadIds.size() + "] account threads to finish");
                    }
                } catch (Exception e) {
                }
            }
            try {
                timer.cancel();
            } catch (Exception e) {
            }
            // save anything that hasn't been saved yet
            bulkSave();
            bulkSaveTBT();
            bulkSaveTCT();
            Date end = new Date();
            Long timeSec = (end.getTime() - start.getTime()) / 1000l;
            logger.info("Processed [" + i + "] accounts in [" + timeSec + "] seconds");

        }
        return "Ok";
    }

    /**
     *
     * @param accountId
     * @param i
     * @return
     */
    public int processLine(String accountId, int i) {
        accountId = accountId.trim();
        if (!accountId.isEmpty()) {
            ++i;
            logger.info("Starting threads for [" + accountId + "]");
            UserMetricAccountThread accountThread = new UserMetricAccountThread(this, accountId, bountyQueryHandler, claimQueryHandler, commentQueryHandler);
            threadIds.add(accountThread.getId());
            Thread thread = new Thread(accountThread);
            thread.start();

            TBTAccountThread tbtThread = new TBTAccountThread(this, accountId, accountingQueryHandler);
            threadIds.add(tbtThread.getId());
            Thread thread2 = new Thread(tbtThread);
            thread2.start();

            TCTAccountThread tctThread = new TCTAccountThread(this, accountId, accountingQueryHandler);
            threadIds.add(tctThread.getId());
            Thread thread3 = new Thread(tctThread);
            thread3.start();
        }

        return i;

    }

    /**
     *
     * @param userMetric
     */
    public void processUserMetric(UserMetric userMetric) {
        synchronized (LOCK) {
            userMetrics.add(userMetric);
        }

        if (userMetrics.size() >= 500) {
            bulkSave();
        }
    }

    /**
     *
     * @param tbts
     */
    public void processTBTs(List<TokenBountyTotal> tbts) {
        synchronized (LOCK_TBT) {
            this.tbts.addAll(tbts);
        }

        if (this.tbts.size() >= 500) {
            bulkSaveTBT();
        }
    }

    /**
     *
     * @param tcts
     */
    public void processTCTs(List<TokenClaimTotal> tcts) {
        synchronized (LOCK_TCT) {
            this.tcts.addAll(tcts);
        }

        if (this.tcts.size() >= 500) {
            bulkSaveTCT();
        }
    }

    /**
     * As each thread finishes, it'll call this method
     *
     * @param id
     */
    public void finishedAccountThread(String id) {
        this.threadIds.remove(id);
    }

    /**
     *
     * @return
     */
    public int getThreadIdsSize() {
        return this.threadIds.size();
    }

    /**
     *
     * @param id
     */
    @Override
    public void processTimeout(UUID id) {
        if (userMetrics.size() > 32) {
            bulkSave();
        }
        if (tbts.size() > 32) {
            bulkSaveTBT();
        }
        if (tcts.size() > 32) {
            bulkSaveTCT();
        }
    }

    /**
     * Bulk save what's been collected
     */
    public void bulkSave() {
        if (!userMetrics.isEmpty()) {
            synchronized (LOCK) {
                BulkRequest.Builder brBounty = new BulkRequest.Builder();
                for (UserMetric userMetric : userMetrics) {
                    ObjectNode node = mapper.convertValue(userMetric, ObjectNode.class);
                    node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.UserMetric");
                    brBounty.operations(op -> op
                            .index(idx -> idx
                            .index(UserMetric.INDEX_NAME)
                            .id(userMetric.getId())
                            .document(node)
                            )
                    );
                }
                try {
                    BulkResponse result = esClient.bulk(brBounty.build());
                    if (result.errors()) {
                        logger.error("Bulk save errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while updating user metrics! [" + ex.getMessage() + "]", ex);
                }
                userMetrics.clear();
            }
        }
    }

    /**
     *
     */
    public void bulkSaveTBT() {
        if (!tbts.isEmpty()) {
            synchronized (LOCK_TBT) {
                BulkRequest.Builder brBounty = new BulkRequest.Builder();
                for (TokenBountyTotal tbt : tbts) {
                    ObjectNode node = mapper.convertValue(tbt, ObjectNode.class);
                    node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.TokenBountyTotal");
                    brBounty.operations(op -> op
                            .index(idx -> idx
                            .index(TokenBountyTotal.INDEX_NAME)
                            .id(tbt.getId())
                            .document(node)
                            )
                    );
                }
                try {
                    BulkResponse result = esClient.bulk(brBounty.build());
                    if (result.errors()) {
                        logger.error("Bulk save errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while updating tbts! [" + ex.getMessage() + "]", ex);
                }
                tbts.clear();
            }
        }
    }

    /**
     *
     */
    public void bulkSaveTCT() {
        if (!tcts.isEmpty()) {
            synchronized (LOCK_TCT) {
                BulkRequest.Builder brBounty = new BulkRequest.Builder();
                for (TokenClaimTotal tct : tcts) {
                    ObjectNode node = mapper.convertValue(tct, ObjectNode.class);
                    node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.TokenClaimTotal");
                    brBounty.operations(op -> op
                            .index(idx -> idx
                            .index(TokenClaimTotal.INDEX_NAME)
                            .id(tct.getId())
                            .document(node)
                            )
                    );
                }
                try {
                    BulkResponse result = esClient.bulk(brBounty.build());
                    if (result.errors()) {
                        logger.error("Bulk save errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while updating tcts! [" + ex.getMessage() + "]", ex);
                }
                tcts.clear();
            }
        }
    }

}
