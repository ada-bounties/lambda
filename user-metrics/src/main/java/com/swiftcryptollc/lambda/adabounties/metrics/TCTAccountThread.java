package com.swiftcryptollc.lambda.adabounties.metrics;

import com.swiftcryptollc.commons.adabounties.data.Timeframe;
import com.swiftcryptollc.commons.adabounties.es.data.Accounting;
import com.swiftcryptollc.commons.adabounties.es.data.TokenClaimTotal;
import com.swiftcryptollc.commons.adabounties.es.query.AccountingQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TCTAccountThread implements Runnable {

    protected UserMetricsService userMetrics;
    protected final String id;
    private final Long timeframe30 = 2592000000L;
    private final Long timeframe60 = 5184000000L;
    private final Long timeframe90 = 7776000000L;
    private final Long timeframe24h = 86400000L;
    private final String accountId;
    private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd");
    private final AccountingQueryHandler accountingQueryHandler;
    protected final static Logger logger = LoggerFactory.getLogger(TCTAccountThread.class);

    /**
     *
     * @param userMetrics
     * @param accountId
     * @param accountingQueryHandler
     */
    public TCTAccountThread(UserMetricsService userMetrics, String accountId, AccountingQueryHandler accountingQueryHandler) {
        this.userMetrics = userMetrics;
        this.accountId = accountId;
        this.accountingQueryHandler = accountingQueryHandler;
        this.id = Base64Id.getNewId();
    }

    /**
     *
     */
    @Override
    public void run() {
        Long nowMs = new Date().getTime();
        try {
            Map<String, TokenClaimTotal> tokenMap30 = new HashMap<>();
            Map<String, TokenClaimTotal> tokenMap60 = new HashMap<>();
            Map<String, TokenClaimTotal> tokenMap90 = new HashMap<>();
            Map<String, TokenClaimTotal> tokenMap24h = new HashMap<>();

            Long endTimeMs30 = nowMs - timeframe30;
            Long endTimeMs60 = nowMs - timeframe60;
            Long endTimeMs90 = nowMs - timeframe90;
            Long endTimeMs24h = nowMs - timeframe24h;
            logger.info("Processing account [" + accountId + "]");

            Integer page = 1;
            do {
                PageRequest pageable = PageRequest.of(page, 100);
                Set<Accounting> accountingSet = accountingQueryHandler.findClaimAccountingRecords(accountId, null, nowMs, endTimeMs90, pageable);
                for (Accounting accounting : accountingSet) {
                    if (accounting.getDateTimeMs() >= endTimeMs24h) {
                        addToMap(accounting, tokenMap24h, Timeframe.DAYS_01.label, nowMs);
                        addToMap(accounting, tokenMap30, Timeframe.DAYS_30.label, nowMs);
                        addToMap(accounting, tokenMap60, Timeframe.DAYS_60.label, nowMs);
                        addToMap(accounting, tokenMap90, Timeframe.DAYS_90.label, nowMs);
                    } else if (accounting.getDateTimeMs() >= endTimeMs30) {
                        addToMap(accounting, tokenMap30, Timeframe.DAYS_30.label, nowMs);
                        addToMap(accounting, tokenMap60, Timeframe.DAYS_60.label, nowMs);
                        addToMap(accounting, tokenMap90, Timeframe.DAYS_90.label, nowMs);
                    } else if (accounting.getDateTimeMs() >= endTimeMs60) {
                        addToMap(accounting, tokenMap60, Timeframe.DAYS_60.label, nowMs);
                        addToMap(accounting, tokenMap90, Timeframe.DAYS_90.label, nowMs);
                    } else {
                        addToMap(accounting, tokenMap90, Timeframe.DAYS_90.label, nowMs);
                    }
                }

                if (accountingSet.size() == 100) {
                    ++page;
                } else {
                    page = -1;
                }
            } while (page > 0);

            userMetrics.processTCTs(new ArrayList<>(tokenMap24h.values()));
            userMetrics.processTCTs(new ArrayList<>(tokenMap30.values()));
            userMetrics.processTCTs(new ArrayList<>(tokenMap60.values()));
            userMetrics.processTCTs(new ArrayList<>(tokenMap90.values()));
        } catch (Exception ex) {
            logger.error("Exception occured while processing account [" + accountId + "] [" + ex.getMessage() + "]");
        }
        logger.info("Finished account [" + accountId + "]");
        userMetrics.finishedAccountThread(id);
    }

    /**
     *
     * @param accounting
     * @param tokenMap
     * @param lastXDays
     * @param nowMs
     */
    private void addToMap(Accounting accounting, Map<String, TokenClaimTotal> tokenMap, String lastXDays, Long nowMs) {
        if (tokenMap.containsKey(accounting.getToken())) {
            TokenClaimTotal tct = tokenMap.get(accounting.getToken());
            tct.setAmount(tct.getAmount() + accounting.getAmount());
            tct.setValueUSD(tct.getValueUSD() + accounting.getValueUSD());
            tokenMap.put(accounting.getToken(), tct);
        } else {
            TokenClaimTotal tct = new TokenClaimTotal();
            tct.setAccountId(accountId);
            tct.setUpdateTimeMs(nowMs);
            tct.setLastXDays(lastXDays);
            tct.setAmount(accounting.getAmount());
            tct.setValueUSD(accounting.getValueUSD());
            tct.setToken(accounting.getToken());
            if (lastXDays.equals(Timeframe.DAYS_01.label)) {
                tct.setId(SDF.format(new Date(nowMs)).concat("-").concat(accountId).concat("-").concat(accounting.getToken()));
            } else {
                tct.setId(accountId.concat("-").concat(lastXDays).concat("-").concat(accounting.getToken()));
            }

            tokenMap.put(accounting.getToken(), tct);
        }
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
}
