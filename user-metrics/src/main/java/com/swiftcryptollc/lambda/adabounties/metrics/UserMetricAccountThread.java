package com.swiftcryptollc.lambda.adabounties.metrics;

import com.swiftcryptollc.commons.adabounties.data.Timeframe;
import com.swiftcryptollc.commons.adabounties.es.data.UserMetric;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.ClaimQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.CommentQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class UserMetricAccountThread implements Runnable {

    protected final String id;
    private final Long[] timeframes = {2592000000L, 5184000000L, 7776000000L, -1L, 86400000L};
    private final UserMetricsService userMetrics;
    private final String accountId;
    private final BountyQueryHandler bountyQueryHandler;
    private final ClaimQueryHandler claimQueryHandler;
    private final CommentQueryHandler commentQueryHandler;
    protected final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    protected final static Logger logger = LoggerFactory.getLogger(UserMetricAccountThread.class);

    /**
     *
     * @param userMetrics
     * @param accountId
     * @param bountyQueryHandler
     * @param claimQueryHandler
     * @param commentQueryHandler
     */
    public UserMetricAccountThread(UserMetricsService userMetrics, String accountId, BountyQueryHandler bountyQueryHandler, ClaimQueryHandler claimQueryHandler, CommentQueryHandler commentQueryHandler) {
        this.userMetrics = userMetrics;
        this.accountId = accountId;
        this.bountyQueryHandler = bountyQueryHandler;
        this.claimQueryHandler = claimQueryHandler;
        this.commentQueryHandler = commentQueryHandler;
        this.id = Base64Id.getNewId();
    }

    /**
     *
     */
    @Override
    public void run() {
        Date now = new Date();
        Long nowMs = now.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        for (int i = 0; i < timeframes.length; ++i) {
            try {
                Long timeframe = timeframes[i];
                Long oldestTimeMs = nowMs;
                if (timeframe > 0) {
                    oldestTimeMs = nowMs - timeframe;
                } else {
                    oldestTimeMs = -1L;
                }
                logger.info("Account [" + accountId + "] from [" + dateTimeFormat.format(new Date(oldestTimeMs)) + "]");
                UserMetric userMetric = new UserMetric();
                userMetric.setUpdateTimeMs(nowMs);
                userMetric.setAccountId(accountId);

                switch (i) {
                    case 0:
                        userMetric.setLastXDays(Timeframe.DAYS_30.toString());
                        userMetric.setId(accountId.concat(userMetric.getLastXDays()));
                        break;
                    case 1:
                        userMetric.setLastXDays(Timeframe.DAYS_60.toString());
                        userMetric.setId(accountId.concat(userMetric.getLastXDays()));
                        break;
                    case 2:
                        userMetric.setLastXDays(Timeframe.DAYS_90.toString());
                        userMetric.setId(accountId.concat(userMetric.getLastXDays()));
                        break;
                    case 3:
                        userMetric.setLastXDays(Timeframe.DAYS_ALL.toString());
                        userMetric.setId(accountId.concat(userMetric.getLastXDays()));
                        break;
                    case 4:
                        // These will not get overwritten
                        userMetric.setLastXDays(Timeframe.DAYS_01.toString());
                        userMetric.setId(sdf.format(now).concat("-").concat(accountId));
                        break;
                }
                // Long totalOpen = bountyQueryHandler.getTotalForBountyStateByAccount(accountId, startTimeMs, BountyStatus.OPEN.label);
                // Long totalClaimed = bountyQueryHandler.getTotalForBountyStateByAccount(accountId, startTimeMs, BountyStatus.CLAIMED.label);
                // Long totalExpired = bountyQueryHandler.getTotalForBountyStateByAccount(accountId, startTimeMs, BountyStatus.EXPIRED.label);
                // Long totalClosed = bountyQueryHandler.getTotalForBountyStateByAccount(accountId, startTimeMs, BountyStatus.CLOSED.label);
                // Long totalStale = bountyQueryHandler.getTotalForBountyStateByAccount(accountId, startTimeMs, BountyStatus.STALE.label);
                // Long total = bountyQueryHandler.getTotalForBountyStateByAccount(accountId, startTimeMs, null);

                bountyQueryHandler.getStatusCountForBountyByAccount(accountId, oldestTimeMs, userMetric);
                claimQueryHandler.getStatusCountForClaimByAccount(accountId, oldestTimeMs, userMetric);
                commentQueryHandler.getStatusCountsForCommentByAccount(accountId, oldestTimeMs, userMetric);
                userMetrics.processUserMetric(userMetric);
            } catch (Exception ex) {
                logger.error("Exception occured while processing account [" + accountId + "] [" + ex.getMessage() + "]");
            }
        }
        logger.info("Finished account [" + accountId + "]");
        userMetrics.finishedAccountThread(id);
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
}
