package com.swiftcryptollc.lambda.adabounties.metrics;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TestUserMetricsService {

    @Test
    public void testService() {
        Logger root = (Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);

        UserMetricsService userMetricsService = new UserMetricsService();
        userMetricsService.processLine("igiGVrRhTtG7nzI4hITXlA", 0);

        try {
            Thread.sleep(10000);
        } catch (Exception e) {
        }
        int sleep = 0;
        while (userMetricsService.getThreadIdsSize() > 0) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
            if (++sleep > 5) {
                System.out.println("Waiting for service to finish");
            }
        }

        userMetricsService.bulkSave();
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
        }
        System.out.println("FINISHED");
    }
}
