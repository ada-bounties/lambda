package com.swiftcryptollc.lambda.adabounties.jwt;

import ch.qos.logback.classic.Level;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import com.swiftcryptollc.commons.adabounties.data.JwtData;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import javax.crypto.SecretKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Run as a "cron job" that is triggered from a event bridge event
 *
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public final class JwtService implements RequestHandler<Object, String> {

    private final static String JWT_KEY = "jwtString";
    protected static String newPreviousJwtString;

    protected final static Logger logger = LoggerFactory.getLogger(JwtService.class);

    static {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        SecretsManager sm = new SecretsManager(true);
    }

    /**
     * Default Constructor
     */
    public JwtService() {
    }

    /**
     * We aren't using the event information since this is a simple time
     * scheduled "cron" job
     *
     * Max Lambda execution time is 15 minutes.
     *
     * @param eventObject
     * @param context
     * @return
     */
    @Override
    public String handleRequest(Object eventObject, Context context) {
        Date start = new Date();
        Long nowMs = start.getTime();
        Gson gson = new GsonBuilder().create();
        SecretKey secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS512);
        byte[] rawSecretKey = secretKey.getEncoded();
        String jwtString = Base64.getEncoder().encodeToString(rawSecretKey);
        JwtData jwt = new JwtData();
        jwt.setJwtString(jwtString);

        Map<String, String> secretsMap = null;
        do {
            secretsMap = SecretsManager.getSecretMap();
            if (secretsMap.isEmpty()) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }

            }
        } while (secretsMap.isEmpty());

        newPreviousJwtString = secretsMap.get(JWT_KEY);
        jwt.setPreviousJwtString(newPreviousJwtString);
        try {
            SecretsManager.putSecrets("jwt", gson.toJson(jwt));
        } catch (Exception ex) {
            logger.error("Could not put the new JWT up! [" + ex.getMessage() + "]", ex);
        }
        Date end = new Date();
        Long timeSec = (end.getTime() - nowMs) / 1000l;
        logger.info("Processed in [" + timeSec + "] seconds");

        return "Ok";
    }
}
