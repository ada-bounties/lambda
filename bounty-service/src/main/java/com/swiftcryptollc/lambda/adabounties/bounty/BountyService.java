package com.swiftcryptollc.lambda.adabounties.bounty;

import ch.qos.logback.classic.Level;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.swiftcryptollc.commons.adabounties.aws.sqs.SqsService;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.utilities.data.ActionType;
import com.swiftcryptollc.commons.adabounties.data.AfterActionData;
import com.swiftcryptollc.commons.adabounties.data.BountyStatus;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.BountyDates;
import com.swiftcryptollc.commons.adabounties.es.data.Claim;
import com.swiftcryptollc.commons.adabounties.es.query.BountyDatesQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.ClaimQueryHandler;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Run as a "cron job" that is triggered from Amazon EventBridge Service to
 * update bounties
 *
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public final class BountyService implements RequestHandler<Object, String> {

    private final static SqsService sqsService;
    private final static LambdaElasticsearchClientConfig esClientConfig;
    private final static ElasticsearchClient esClient;
    private final static BountyDatesQueryHandler btyDatesQueryHandler;
    private final static BountyQueryHandler btyHandler;
    private final static ClaimQueryHandler clmHandler;
    private final static Long THREE_MONTHS = 7776000000L;
    protected final static Logger logger = LoggerFactory.getLogger(BountyService.class);

    static {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        esClientConfig = new LambdaElasticsearchClientConfig();
        esClient = esClientConfig.elasticSearchClient();
        btyDatesQueryHandler = new BountyDatesQueryHandler();
        btyDatesQueryHandler.setClient(esClient);
        btyHandler = new BountyQueryHandler();
        btyHandler.setClient(esClient);
        clmHandler = new ClaimQueryHandler();
        clmHandler.setClient(esClient);
        sqsService = new SqsService(true);
    }

    /**
     * Default Constructor
     */
    public BountyService() {
    }

    /**
     * We aren't using the event information since this is a simple time
     * scheduled "cron" job
     *
     * Max Lambda execution time is 15 minutes.
     *
     * @param eventObject
     * @param context
     * @return
     */
    @Override
    public String handleRequest(Object eventObject, Context context) {
        ObjectMapper mapper = new ObjectMapper();
        Long now = new Date().getTime();
        Long lastIntMs = now - THREE_MONTHS;

        logger.info("Searching for expired and stale bounties");
        try {
            Set<BountyDates> bountySet = btyDatesQueryHandler.findStaleOrExpiredBounties(lastIntMs, now);
            Set<String> ids = new HashSet<>();
            if (!bountySet.isEmpty()) {
                logger.info("Processing [" + bountySet.size() + "] bounties");
                Boolean hasStale = false;
                Boolean hasDelete = false;
                BulkRequest.Builder br = new BulkRequest.Builder();
                BulkRequest.Builder brDelete = new BulkRequest.Builder();
                for (BountyDates btyDates : bountySet) {
                    if (btyDates.getExpireMs() != null) {
                        ids.add(btyDates.getId());
                        if (btyDates.getExpireMs() < now) {
                            hasDelete = true;
                            logger.info("Expired [" + btyDates.getId() + "]");
                            brDelete.operations(op -> op
                                    .delete(idx -> idx
                                    .index(BountyDates.INDEX_NAME)
                                    .id(btyDates.getId())
                                    )
                            );
                        } else if (btyDates.getLastIntMs() < lastIntMs) {
                            hasStale = true;
                            btyDates.setLastIntMs(-1L);
                            ObjectNode node = mapper.convertValue(btyDates, ObjectNode.class);
                            node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.BountyDates");
                            logger.info("Stale [" + btyDates.getId() + "]");
                            br.operations(op -> op
                                    .index(idx -> idx
                                    .index(BountyDates.INDEX_NAME)
                                    .id(btyDates.getId())
                                    .document(node)
                                    )
                            );
                        }
                    } else {
                        logger.info("Removing Bounty Dates with a null expired date");
                        brDelete.operations(op -> op
                                .delete(idx -> idx
                                .index(BountyDates.INDEX_NAME)
                                .id(btyDates.getId())
                                )
                        );
                    }
                }

                Set<String> processedIds = new HashSet<>();
                Set<Bounty> bounties = btyHandler.findAllById(ids);
                if (!bounties.isEmpty()) {
                    logger.info("Processing [" + bounties.size() + "] bounty updates");
                    BulkRequest.Builder brBounty = new BulkRequest.Builder();
                    for (Bounty bty : bounties) {
                        if ((bty.getStatus().equals(BountyStatus.OPEN.label))
                                || (bty.getStatus().equals(BountyStatus.STALE.label))) {
                            processedIds.add(bty.getId());
                            bty.setUpdatedMs(now);
                            if (bty.getExpireMs() < now) {
                                logger.info("Marked [" + bty.getId() + "] EXPIRED");
                                bty.setStatus(BountyStatus.EXPIRED.label);
                            } else {
                                logger.info("Marked [" + bty.getId() + "] STALE");
                                bty.setStatus(BountyStatus.STALE.label);
                            }
                            ObjectNode node = mapper.convertValue(bty, ObjectNode.class);
                            node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Bounty");
                            brBounty.operations(op -> op
                                    .index(idx -> idx
                                    .index(Bounty.INDEX_NAME)
                                    .id(bty.getId())
                                    .document(node)
                                    )
                            );
                            sqsService.addToQueue(new SqsData(new AfterActionData(ActionType.BOUNTY_UPDATE, bty)));
                        } else {
                            logger.info("Skipping bounty [" + bty.getId() + "] with Status [" + bty.getStatus() + "]");
                        }
                    }
                    if (!processedIds.isEmpty()) {
                        logger.info("Finishing [" + processedIds.size() + "] bounty updates");
                        Boolean updatedBounties = false;
                        try {
                            BulkResponse result = esClient.bulk(brBounty.build());
                            if (result.errors()) {
                                logger.error("Bulk save errors encountered");
                                for (BulkResponseItem item : result.items()) {
                                    if (item.error() != null) {
                                        logger.error(item.error().reason());
                                    }
                                }
                            } else {
                                updatedBounties = true;
                                logger.info("Successful bounty updates");
                            }
                        } catch (Exception ex) {
                            logger.error("Exception occured while updating the stale bounties! [" + ex.getMessage() + "]", ex);
                        }

                        if (updatedBounties) {
                            logger.info("Updating outstanding claims.");
                            Set<Claim> claims = clmHandler.findAllByBountyId(processedIds);
                            if (!claims.isEmpty()) {
                                BulkRequest.Builder brClaims = new BulkRequest.Builder();
                                for (Claim claim : claims) {
                                    claim.setUpdatedMs(now);
                                    claim.setStatus(BountyStatus.CLOSED.label);
                                    ObjectNode node = mapper.convertValue(claim, ObjectNode.class);
                                    node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Claim");
                                    brClaims.operations(op -> op
                                            .index(idx -> idx
                                            .index(Claim.INDEX_NAME)
                                            .id(claim.getId())
                                            .document(node)
                                            )
                                    );
                                    sqsService.addToQueue(new SqsData(new AfterActionData(ActionType.CLAIM_CLOSED, claim)));
                                }
                                try {
                                    BulkResponse result = esClient.bulk(brClaims.build());
                                    if (result.errors()) {
                                        logger.error("Bulk save errors encountered");
                                        for (BulkResponseItem item : result.items()) {
                                            if (item.error() != null) {
                                                logger.error(item.error().reason());
                                            }
                                        }
                                    }
                                } catch (Exception ex) {
                                    logger.error("Exception occured while updating the stale and expired claims! [" + ex.getMessage() + "]", ex);
                                }
                            } else {
                                logger.info("No claims to update");
                            }
                            if (hasDelete) {
                                logger.info("Deleting closed bounty dates");
                                try {
                                    BulkResponse result = esClient.bulk(brDelete.build());
                                    if (result.errors()) {
                                        logger.error("Bulk delete errors encountered");
                                        for (BulkResponseItem item : result.items()) {
                                            if (item.error() != null) {
                                                logger.error(item.error().reason());
                                            }
                                        }
                                    }
                                } catch (Exception ex) {
                                    logger.error("Exception occured while deleting the stale bounty metadata! [" + ex.getMessage() + "]", ex);
                                }
                            } else {
                                logger.info("No Bounty Dates to delete");
                            }
                            if (hasStale) {
                                logger.info("Setting last interaction to -1 for stale bounty dates");
                                try {
                                    BulkResponse result = esClient.bulk(br.build());
                                    if (result.errors()) {
                                        logger.error("Bulk save errors encountered");
                                        for (BulkResponseItem item : result.items()) {
                                            if (item.error() != null) {
                                                logger.error(item.error().reason());
                                            }
                                        }
                                    }
                                } catch (Exception ex) {
                                    logger.error("Exception occured while updating the stale bounty metadata! [" + ex.getMessage() + "]", ex);
                                }
                            } else {
                                logger.info("No stale bounties");
                            }
                        } else {
                            logger.info("Bounties were not updated");
                        }
                    } else {
                        logger.info("No IDs to process!");
                    }
                    logger.info("Finished!");
                }
            } else {
                logger.info("No bounties to process!");
            }
        } catch (Exception ex) {
            logger.error("Error occured during processing! [" + ex.getMessage() + "]", ex);
        }
        Date end = new Date();
        Long timeSec = (end.getTime() - now) / 1000l;
        logger.info("Processed expired and stale bounties in [" + timeSec + "] seconds");
        sqsService.destroy();

        return "Ok";
    }
}
