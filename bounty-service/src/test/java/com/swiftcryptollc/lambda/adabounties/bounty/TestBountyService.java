package com.swiftcryptollc.lambda.adabounties.bounty;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class TestBountyService {

    public static void main(String[] args) {
        BountyService bs = new BountyService();
        bs.handleRequest(null, null);

        while (true) {
            try {
                Thread.sleep(60000);
            } catch (Exception e) {
            }
        }
    }
}
