package com.swiftcryptollc.lambda.adabounties.metrics;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class RunSystemMetrics {

    public static void main(String[] args) {
        SystemMetricsService sms = new SystemMetricsService();
        sms.handleRequest(null, null);

        while (true) {
            try {
                Thread.sleep(30000);
            } catch (Exception e) {
            }
        }
    }
}
