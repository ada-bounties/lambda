package com.swiftcryptollc.lambda.adabounties.metrics;

import com.swiftcryptollc.commons.adabounties.es.data.SystemMetric;
import com.swiftcryptollc.commons.adabounties.es.query.AccountQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.ClaimQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class SystemMetricThread implements Runnable {

    protected final String id;
    private final Long timeframeMs;
    private final SystemMetric systemMetric;
    private final SystemMetricsService systemMetrics;
    private final BountyQueryHandler bountyQueryHandler;
    private final ClaimQueryHandler claimQueryHandler;
    private final AccountQueryHandler accountQueryHandler;
    protected final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    protected final static Logger logger = LoggerFactory.getLogger(SystemMetricThread.class);

    /**
     *
     * @param systemMetrics
     * @param systemMetric
     * @param timeframeMs
     * @param bountyQueryHandler
     * @param claimQueryHandler
     * @param accountQueryHandler
     */
    public SystemMetricThread(SystemMetricsService systemMetrics, SystemMetric systemMetric, Long timeframeMs, BountyQueryHandler bountyQueryHandler, ClaimQueryHandler claimQueryHandler, AccountQueryHandler accountQueryHandler) {
        this.systemMetrics = systemMetrics;
        this.systemMetric = systemMetric;
        this.timeframeMs = timeframeMs;
        this.bountyQueryHandler = bountyQueryHandler;
        this.claimQueryHandler = claimQueryHandler;
        this.accountQueryHandler = accountQueryHandler;
        this.id = Base64Id.getNewId();
    }

    /**
     *
     */
    @Override
    public void run() {
        Long lessThanTimeMs = new Date().getTime();
        try {
            Long greaterThanTimeMs;
            if (timeframeMs > 0) {
                greaterThanTimeMs = lessThanTimeMs - timeframeMs;
            } else {
                greaterThanTimeMs = -1L;
            }
            logger.info("[" + systemMetric.getLastXDays() + "] Collecting system metrics from [" + dateTimeFormat.format(new Date(greaterThanTimeMs)) + "] to [" + dateTimeFormat.format(new Date(lessThanTimeMs)) + "]");
            bountyQueryHandler.getStatusCountForBounty(lessThanTimeMs, greaterThanTimeMs, timeframeMs, systemMetric);
            claimQueryHandler.getStatusCountForClaim(lessThanTimeMs, greaterThanTimeMs, timeframeMs, systemMetric);
            accountQueryHandler.getTotalAccountsFrom(lessThanTimeMs, greaterThanTimeMs, timeframeMs, systemMetric);
            systemMetrics.processSystemMetric(systemMetric);
        } catch (Exception ex) {
            logger.error("Exception occured while processing timeframe [" + timeframeMs + "] [" + ex.getMessage() + "]");
        }
        systemMetrics.finishedTimeThread(id);
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
}
