package com.swiftcryptollc.lambda.adabounties.metrics;

import ch.qos.logback.classic.Level;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.swiftcryptollc.commons.adabounties.aws.sqs.SqsService;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.data.Timeframe;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.data.SystemMetric;
import com.swiftcryptollc.commons.adabounties.es.query.AccountQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.ClaimQueryHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Run as a "cron job" that is triggered from a event bridge event
 *
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public final class SystemMetricsService implements RequestHandler<Object, String> {

    private final static SqsService sqsService;
    private final ObjectMapper mapper = new ObjectMapper();
    private final Set<String> threadIds = new HashSet<>();
    private final Set<SystemMetric> systemMetrics = new HashSet<>();
    private final static LambdaElasticsearchClientConfig esClientConfig;
    private final static ElasticsearchClient esClient;
    private final static BountyQueryHandler bountyQueryHandler;
    private final static ClaimQueryHandler claimQueryHandler;
    private final static AccountQueryHandler accountQueryHandler;
    private final static Object LOCK = new Object();
    protected UUID id;
    private final Long[] timeframes = {2592000000L, 5184000000L, 7776000000L, -1L, 86400000L};
    protected final SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    protected final static Logger logger = LoggerFactory.getLogger(SystemMetricsService.class);

    static {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        esClientConfig = new LambdaElasticsearchClientConfig();
        esClient = esClientConfig.elasticSearchClient();
        bountyQueryHandler = new BountyQueryHandler();
        bountyQueryHandler.setClient(esClient);
        claimQueryHandler = new ClaimQueryHandler();
        claimQueryHandler.setClient(esClient);
        accountQueryHandler = new AccountQueryHandler();
        accountQueryHandler.setClient(esClient);
        sqsService = new SqsService(true);
    }

    /**
     * Default Constructor
     */
    public SystemMetricsService() {
    }

    /**
     * We aren't using the event information since this is a simple time
     * scheduled "cron" job
     *
     * Max Lambda execution time is 15 minutes.
     *
     * @param eventObject
     * @param context
     * @return
     */
    @Override
    public String handleRequest(Object eventObject, Context context) {
        Date start = new Date();
        Long nowMs = start.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        for (int i = 0; i < timeframes.length; ++i) {
            try {
                Long timeframe = timeframes[i];
                Long oldestTimeMs = nowMs;
                if (timeframe > 0) {
                    oldestTimeMs = nowMs - timeframe;
                } else {
                    oldestTimeMs = -1L;
                }
                logger.info("System metrics from [" + dateTimeFormat.format(oldestTimeMs) + "]");
                SystemMetric systemMetric = new SystemMetric();
                systemMetric.setUpdateTimeMs(nowMs);
                switch (i) {
                    case 0:
                        systemMetric.setId("metric-".concat(Timeframe.DAYS_30.toString()));
                        systemMetric.setLastXDays(Timeframe.DAYS_30.toString());
                        break;
                    case 1:
                        systemMetric.setId("metric-".concat(Timeframe.DAYS_60.toString()));
                        systemMetric.setLastXDays(Timeframe.DAYS_60.toString());
                        break;
                    case 2:
                        systemMetric.setId("metric-".concat(Timeframe.DAYS_90.toString()));
                        systemMetric.setLastXDays(Timeframe.DAYS_90.toString());
                        break;
                    case 3:
                        systemMetric.setId("metric-".concat(Timeframe.DAYS_ALL.toString()));
                        systemMetric.setLastXDays(Timeframe.DAYS_ALL.toString());
                        break;
                    case 4:
                        // These will not get overwritten
                        systemMetric.setId(sdf.format(start));
                        systemMetric.setLastXDays(Timeframe.DAYS_01.toString());
                        break;
                }

                SystemMetricThread sysThread = new SystemMetricThread(this, systemMetric, timeframes[i], bountyQueryHandler, claimQueryHandler, accountQueryHandler);
                this.threadIds.add(sysThread.getId());
                new Thread(sysThread).start();
            } catch (Exception ex) {
                logger.error("Exception occured while processing system metrics [" + ex.getMessage() + "]");
            }
        }
        int sleepCtr = 0;
        while (!threadIds.isEmpty()) {
            try {
                Thread.sleep(500);
                if (++sleepCtr > 60) {
                    logger.info("Waiting for [" + threadIds.size() + "] threads to finish");
                }
            } catch (Exception e) {
            }
        }
        // save anything that hasn't been saved yet
        bulkSave();
        SqsData data = new SqsData(new SystemMetric());
        data.setSendToAll(Boolean.TRUE);
        sqsService.addToQueue(data);
        sqsService.destroy();
        Date end = new Date();
        Long timeSec = (end.getTime() - start.getTime()) / 1000l;
        logger.info("Processed in [" + timeSec + "] seconds");

        return "Ok";
    }

    /**
     *
     * @param systemMetric
     */
    public void processSystemMetric(SystemMetric systemMetric) {
        synchronized (LOCK) {
            systemMetrics.add(systemMetric);
        }
    }

    /**
     * As each thread finishes, it'll call this method
     *
     * @param id
     */
    public void finishedTimeThread(String id) {
        this.threadIds.remove(id);
    }

    /**
     *
     * @return
     */
    public int getThreadIdsSize() {
        return this.threadIds.size();
    }

    /**
     * Bulk save what's been collected
     */
    public synchronized void bulkSave() {
        if (!systemMetrics.isEmpty()) {
            synchronized (LOCK) {
                BulkRequest.Builder brSystemMetrics = new BulkRequest.Builder();
                for (SystemMetric systemMetric : systemMetrics) {
                    ObjectNode node = mapper.convertValue(systemMetric, ObjectNode.class);
                    node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.SystemMetric");
                    brSystemMetrics.operations(op -> op
                            .index(idx -> idx
                            .index(SystemMetric.INDEX_NAME)
                            .id(systemMetric.getId())
                            .document(node)
                            )
                    );
                }
                try {
                    BulkResponse result = esClient.bulk(brSystemMetrics.build());
                    if (result.errors()) {
                        logger.error("Bulk save errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while updating system metrics! [" + ex.getMessage() + "]", ex);
                }
                systemMetrics.clear();
            }
        }
    }
}
