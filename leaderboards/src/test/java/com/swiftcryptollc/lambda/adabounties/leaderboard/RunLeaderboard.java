package com.swiftcryptollc.lambda.adabounties.leaderboard;

import com.swiftcryptollc.lambda.adabounties.leaderboards.LeaderboardService;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class RunLeaderboard {

    public static void main(String[] args) {
        LeaderboardService ls = new LeaderboardService();
        ls.handleRequest(null, null);

        while (true) {
            try {
                Thread.sleep(30000);
            } catch (Exception e) {
            }
        }
    }
}
