package com.swiftcryptollc.lambda.adabounties.leaderboards;

import com.swiftcryptollc.commons.adabounties.data.LeaderboardEnum;
import com.swiftcryptollc.commons.adabounties.data.Timeframe;
import com.swiftcryptollc.commons.adabounties.es.data.Leaderboard;
import com.swiftcryptollc.commons.adabounties.es.data.LeaderboardEntry;
import com.swiftcryptollc.commons.adabounties.es.data.UserMetric;
import com.swiftcryptollc.commons.adabounties.es.query.LeaderboardQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.UserMetricQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.data.SyncArrayDeque;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class LeaderboardUserMetricThread extends AbstractLeaderboardThread {

    private final UserMetricQueryHandler userMetricQueryHandler;
    protected final static Logger logger = LoggerFactory.getLogger(LeaderboardUserMetricThread.class);

    public LeaderboardUserMetricThread(LeaderboardEnum type, LeaderboardService leaderboardService, UserMetricQueryHandler userMetricQueryHandler, LeaderboardQueryHandler leaderQueryHandler, String ymd, String yymd, Integer pageSize) {
        super(type, leaderboardService, leaderQueryHandler, ymd, yymd, null, pageSize);
        this.userMetricQueryHandler = userMetricQueryHandler;

    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            logger.info("Collecting user metric leaders");
            PageRequest pageable = PageRequest.of(1, pageSize);
            String sortField = null;

            switch (type) {
                case OPEN_BOUNTIES ->
                    sortField = "openBounties";
                case CLAIMED_BOUNTIES ->
                    sortField = "claimedBounties";
                case EXPIRED_BOUNTIES ->
                    sortField = "expiredBounties";
                case STALE_BOUNTIES ->
                    sortField = "staleBounties";
                case TOTAL_BOUNTIES ->
                    sortField = "totalBounties";
                case OPEN_CLAIMS ->
                    sortField = "openClaims";
                case ACCEPTED_CLAIMS ->
                    sortField = "acceptedClaims";
                case REJ_CLAIMS ->
                    sortField = "rejectedClaims";
                case TOTAL_CLAIMS ->
                    sortField = "totalClaims";
            }
            Leaderboard leaderboard = initToday();
            Integer rank = 1;
            SyncArrayDeque<UserMetric> userMetrics = userMetricQueryHandler.searchUserMetrics(Timeframe.DAYS_ALL.label, sortField, pageable);
            UserMetric userMetric = null;
            Double value;
            Double preValue = Double.MIN_VALUE;
            Integer preRank = 1;
            LeaderboardEntry lbe;
            while ((userMetric = userMetrics.pollFirst()) != null) {
                String accountId = userMetric.getAccountId();

                switch (type) {
                    case OPEN_BOUNTIES:
                        value = Double.valueOf(userMetric.getOpenBounties());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case CLAIMED_BOUNTIES:
                        value = Double.valueOf(userMetric.getClaimedBounties());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case EXPIRED_BOUNTIES:
                        value = Double.valueOf(userMetric.getExpiredBounties());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case STALE_BOUNTIES:
                        value = Double.valueOf(userMetric.getStaleBounties());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case TOTAL_BOUNTIES:
                        value = Double.valueOf(userMetric.getTotalBounties());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case OPEN_CLAIMS:
                        value = Double.valueOf(userMetric.getOpenClaims());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case ACCEPTED_CLAIMS:
                        value = Double.valueOf(userMetric.getAcceptedClaims());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case REJ_CLAIMS:
                        value = Double.valueOf(userMetric.getRejectedClaims());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case TOTAL_CLAIMS:
                        value = Double.valueOf(userMetric.getTotalClaims());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                }
            }
            leaderboardService.processLeaderboard(leaderboard);

        } catch (Exception ex) {
            logger.error("Exception occured while processing user metric leaderboard [" + ex.getMessage() + "]");
        }
        leaderboardService.finishedTimeThread(id);
    }
}
