package com.swiftcryptollc.lambda.adabounties.leaderboards;

import com.swiftcryptollc.commons.adabounties.data.LeaderboardEnum;
import com.swiftcryptollc.commons.adabounties.data.Timeframe;
import com.swiftcryptollc.commons.adabounties.es.data.Leaderboard;
import com.swiftcryptollc.commons.adabounties.es.data.LeaderboardEntry;
import com.swiftcryptollc.commons.adabounties.es.data.TokenBountyTotal;
import com.swiftcryptollc.commons.adabounties.es.query.LeaderboardQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.TBTQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.data.SyncArrayDeque;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class LeaderboardTBTThread extends AbstractLeaderboardThread {

    private final TBTQueryHandler tbtQueryHandler;
    protected final static Logger logger = LoggerFactory.getLogger(LeaderboardTBTThread.class);

    public LeaderboardTBTThread(LeaderboardEnum type, LeaderboardService leaderboardService,
            TBTQueryHandler tbtQueryHandler, LeaderboardQueryHandler leaderQueryHandler, String ymd,
            String yymd, String token, Integer pageSize) {
        super(type, leaderboardService, leaderQueryHandler, ymd, yymd, token, pageSize);
        this.tbtQueryHandler = tbtQueryHandler;
    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            logger.info("Collecting TBT leaders");
            Leaderboard leaderboard = initToday();
            PageRequest pageable = PageRequest.of(1, pageSize);
            Double preValue = Double.MIN_VALUE;
            Integer preRank = 1;
            LeaderboardEntry lbe;
            Integer rank = 1;
            SyncArrayDeque<TokenBountyTotal> tbtSet = tbtQueryHandler.findTokenBountyTotals(null, token, Timeframe.DAYS_ALL.label, pageable);
            TokenBountyTotal tbt = null;
            while ((tbt = tbtSet.pollFirst()) != null) {
                Double value = tbt.getAmount();
                lbe = processEntry(leaderboard, tbt.getAccountId(), preRank, rank, preValue, value);
                preValue = lbe.getValue();
                preRank = lbe.getRank();
                ++rank;
            }
            leaderboardService.processLeaderboard(leaderboard);
        } catch (Exception ex) {
            logger.error("Exception occured while processing TBT leaderboard [" + ex.getMessage() + "]");
        }
        leaderboardService.finishedTimeThread(id);
    }
}
