package com.swiftcryptollc.lambda.adabounties.leaderboards;

import com.swiftcryptollc.commons.adabounties.data.LeaderboardEnum;
import com.swiftcryptollc.commons.adabounties.es.data.Leaderboard;
import com.swiftcryptollc.commons.adabounties.es.data.LeaderboardEntry;
import com.swiftcryptollc.commons.adabounties.es.data.Reputation;
import com.swiftcryptollc.commons.adabounties.es.query.LeaderboardQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.ReputationQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.data.SyncArrayDeque;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class LeaderboardReputationThread extends AbstractLeaderboardThread {

    private final ReputationQueryHandler repQueryHandler;
    protected final static Logger logger = LoggerFactory.getLogger(LeaderboardReputationThread.class);

    public LeaderboardReputationThread(LeaderboardEnum type, LeaderboardService leaderboardService,
            ReputationQueryHandler repQueryHandler, LeaderboardQueryHandler leaderQueryHandler, String ymd,
            String yymd, Integer pageSize) {
        super(type, leaderboardService, leaderQueryHandler, ymd, yymd, null, pageSize);
        this.repQueryHandler = repQueryHandler;
    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            logger.info("Collecting reputation leaders");
            PageRequest pageable = PageRequest.of(1, pageSize);
            String sortField = null;

            switch (type) {
                case REPUTATION ->
                    sortField = "score";
                case BOUNTY_COMMENTS ->
                    sortField = "numCmt";
                case BOUNTY_UPVOTES ->
                    sortField = "btyUp";
                case BOUNTY_DOWNVOTES ->
                    sortField = "btyDown";
                case COMMENT_UPVOTES ->
                    sortField = "cmtUp";
                case COMMENT_DOWNVOTES ->
                    sortField = "cmtDown";
                case REPLYS_TO_COMMENTS ->
                    sortField = "numRplysToCmts";
                case CLAIMS_ON_BOUNTIES ->
                    sortField = "numClmOnBtys";
            }
            Leaderboard leaderboard = initToday();
            Double value;
            Double preValue = Double.MIN_VALUE;
            Integer preRank = 1;
            LeaderboardEntry lbe;
            Integer rank = 1;
            Reputation rep = null;
            SyncArrayDeque<Reputation> reps = repQueryHandler.searchReputations(sortField, pageable);
            while ((rep = reps.pollFirst()) != null) {
                String accountId = rep.getId();
                switch (type) {
                    case REPUTATION:
                        value = rep.getScore();
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case BOUNTY_COMMENTS:
                        value = Double.valueOf(rep.getNumCmt());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case BOUNTY_UPVOTES:
                        value = Double.valueOf(rep.getBtyUp());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case BOUNTY_DOWNVOTES:
                        value = Double.valueOf(rep.getBtyDown());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case COMMENT_UPVOTES:
                        value = Double.valueOf(rep.getCmtUp());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case COMMENT_DOWNVOTES:
                        value = Double.valueOf(rep.getCmtDown());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case REPLYS_TO_COMMENTS:
                        value = Double.valueOf(rep.getNumRplysToCmts());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                    case CLAIMS_ON_BOUNTIES:
                        value = Double.valueOf(rep.getNumClmOnBtys());
                        lbe = processEntry(leaderboard, accountId, preRank, rank, preValue, value);
                        preValue = lbe.getValue();
                        preRank = lbe.getRank();
                        ++rank;
                        break;
                }
            }
            leaderboardService.processLeaderboard(leaderboard);
        } catch (Exception ex) {
            logger.error("Exception occured while processing reputation leaderboard [" + ex.getMessage() + "]");
        }
        leaderboardService.finishedTimeThread(id);
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
}
