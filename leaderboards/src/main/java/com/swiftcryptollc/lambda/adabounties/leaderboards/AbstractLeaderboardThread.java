package com.swiftcryptollc.lambda.adabounties.leaderboards;

import com.swiftcryptollc.commons.adabounties.data.LeaderboardEnum;
import com.swiftcryptollc.commons.adabounties.es.data.Leaderboard;
import com.swiftcryptollc.commons.adabounties.es.data.LeaderboardEntry;
import com.swiftcryptollc.commons.adabounties.es.query.LeaderboardQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.data.Base64Id;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public abstract class AbstractLeaderboardThread implements Runnable {

    protected final String id;
    protected final Integer pageSize;
    protected final String ymd;
    protected final String yymd;
    protected final String token;
    protected final LeaderboardEnum type;
    protected final LeaderboardService leaderboardService;
    protected final LeaderboardQueryHandler leaderQueryHandler;
    private Map<String, Integer> yesterdayRank = new HashMap<>();
    private final static Object LOCK = new Object();
    private static Map<String, Leaderboard> yesterdayMap = null;
    protected final static Logger logger = LoggerFactory.getLogger(AbstractLeaderboardThread.class);

    public AbstractLeaderboardThread(LeaderboardEnum type, LeaderboardService leaderboardService, LeaderboardQueryHandler leaderQueryHandler, String ymd, String yymd, String token, Integer pageSize) {
        this.type = type;
        this.leaderboardService = leaderboardService;
        this.leaderQueryHandler = leaderQueryHandler;
        this.ymd = ymd;
        this.yymd = yymd;
        this.token = token;
        this.pageSize = pageSize;
        this.id = Base64Id.getNewId();
    }

    /**
     *
     */
    public void init() {
        setupYesterday();
        Thread me = new Thread(this);
        me.start();
    }

    /**
     * null return means they were unranked
     *
     * @param accountId
     * @return
     */
    private Integer getYesterdaysRank(String accountId) {
        return this.yesterdayRank.get(accountId);
    }

    /**
     *
     * @return
     */
    public Leaderboard initToday() {
        Leaderboard leaderboard = new Leaderboard();
        leaderboard.setYmd(ymd);
        leaderboard.setType(type.label);
        leaderboard.setId(ymd.concat("-").concat(type.label));
        return leaderboard;
    }

    /**
     *
     * @param accountId
     * @param rank
     * @param value
     * @return
     */
    public LeaderboardEntry getEntry(String accountId, Integer rank, Double value) {
        LeaderboardEntry lbe = new LeaderboardEntry();
        lbe.setAccountId(accountId);
        lbe.setRank(rank);
        lbe.setValue(value);
        lbe.setPrevRank(getYesterdaysRank(accountId));
        return lbe;
    }

    /**
     *
     * @return
     */
    private void setupYesterday() {
        synchronized (LOCK) {
            try {
                if (yesterdayMap == null) {
                    yesterdayMap = new HashMap<>();
                    Set<Leaderboard> lbs = leaderQueryHandler.getAllLeaderboards(yymd);
                    logger.info("Found [" + lbs.size() + "] LBs from yesterday");
                    for (Leaderboard lb : lbs) {
                        yesterdayMap.put(lb.getType(), lb);
                        logger.info("Adding [" + lb.getType() + "]");
                    }
                }
            } catch (Exception e) {
                logger.error("Could not pull yesterdays leaderboard [" + e.getMessage() + "]");
            }
        }
        if (yesterdayMap.containsKey(type.label)) {
            logger.info("Using yesterday's leaderboard for [" + type.label + "]");
            Leaderboard lb = yesterdayMap.get(type.label);
            for (LeaderboardEntry lbe : lb.getEntries()) {
                yesterdayRank.put(lbe.getAccountId(), lbe.getRank());
            }
        } else {
            logger.info("Could not find yesterday's leaderboard for [" + type.label + "]");
        }
    }

    /**
     *
     * @param leaderboard
     * @param accountId
     * @param preRank
     * @param rank
     * @param preValue
     * @param value
     * @return
     */
    public LeaderboardEntry processEntry(Leaderboard leaderboard, String accountId, Integer preRank, Integer rank, Double preValue, Double value) {
        Integer assignRank;
        if (value.equals(preValue)) {
            assignRank = preRank;
        } else {
            assignRank = rank;
        }
        return leaderboard.addEntry(getEntry(accountId, assignRank, value));
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
}
