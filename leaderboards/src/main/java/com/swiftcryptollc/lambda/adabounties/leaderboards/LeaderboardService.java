package com.swiftcryptollc.lambda.adabounties.leaderboards;

import ch.qos.logback.classic.Level;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import com.swiftcryptollc.commons.adabounties.aws.sqs.SqsService;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.data.LeaderboardEnum;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.data.Leaderboard;
import com.swiftcryptollc.commons.adabounties.es.query.LeaderboardQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.ReputationQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.TBTQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.TCTQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.UserMetricQueryHandler;
import com.swiftcryptollc.commons.adabounties.integrations.twitter.TwitterHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Run as a "cron job" that is triggered from a event bridge event
 *
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public final class LeaderboardService implements RequestHandler<Object, String> {
    
    private final static SqsService sqsService;
    private final ObjectMapper mapper = new ObjectMapper();
    private final Set<String> threadIds = new HashSet<>();
    private final Set<Leaderboard> lbs = new HashSet<>();
    private final static LambdaElasticsearchClientConfig esClientConfig;
    private final static ElasticsearchClient esClient;
    private final static ReputationQueryHandler repQueryHandler;
    private final static UserMetricQueryHandler userMetricQueryHandler;
    private final static LeaderboardQueryHandler leaderQueryHandler;
    private final static TBTQueryHandler tbtQueryHandler;
    private final static TCTQueryHandler tctQueryHandler;
    private final static TwitterHandler twitterHandler;
    private final static Object LOCK = new Object();
    protected UUID id;
    private final static Long ONE_DAY_AGO = 86400000L;
    private final static Integer PAGE_SIZE = 50;
    private final static SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd");
    
    protected final static Logger logger = LoggerFactory.getLogger(LeaderboardService.class);
    
    static {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        esClientConfig = new LambdaElasticsearchClientConfig();
        esClient = esClientConfig.elasticSearchClient();
        repQueryHandler = new ReputationQueryHandler();
        repQueryHandler.setClient(esClient);
        userMetricQueryHandler = new UserMetricQueryHandler();
        userMetricQueryHandler.setClient(esClient);
        leaderQueryHandler = new LeaderboardQueryHandler();
        leaderQueryHandler.setClient(esClient);
        tbtQueryHandler = new TBTQueryHandler();
        tbtQueryHandler.setClient(esClient);
        tctQueryHandler = new TCTQueryHandler();
        tctQueryHandler.setClient(esClient);
        twitterHandler = new TwitterHandler();
        twitterHandler.updateSecrets(SecretsManager.getSecretMap());
        sqsService = new SqsService(true);
    }

    /**
     * Default Constructor
     */
    public LeaderboardService() {
    }

    /**
     * We aren't using the event information since this is a simple time
     * scheduled "cron" job
     *
     * Max Lambda execution time is 15 minutes.
     *
     * @param eventObject
     * @param context
     * @return
     */
    @Override
    public String handleRequest(Object eventObject, Context context) {
        Date start = new Date();
        Date yesterday = new Date(start.getTime() - ONE_DAY_AGO);
        String ymd = SDF.format(start);
        String yymd = SDF.format(yesterday);
        logger.info("Procssing LBs for [" + ymd + "] with yesterday [" + yymd + "]");
        LeaderboardTBTThread tbtThread = new LeaderboardTBTThread(LeaderboardEnum.TBT_ADA, this, tbtQueryHandler, leaderQueryHandler, ymd, yymd, "ADA", PAGE_SIZE);
        tbtThread.init();
        threadIds.add(tbtThread.getId());
        LeaderboardTCTThread tctThread = new LeaderboardTCTThread(LeaderboardEnum.TCT_ADA, this, tctQueryHandler, leaderQueryHandler, ymd, yymd, "ADA", PAGE_SIZE);
        tctThread.init();
        threadIds.add(tctThread.getId());
        LeaderboardReputationThread repThread = new LeaderboardReputationThread(LeaderboardEnum.REPUTATION, this, repQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        repThread.init();
        threadIds.add(repThread.getId());
        LeaderboardReputationThread repThread2 = new LeaderboardReputationThread(LeaderboardEnum.BOUNTY_COMMENTS, this, repQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        repThread2.init();
        threadIds.add(repThread2.getId());
        LeaderboardReputationThread repThread3 = new LeaderboardReputationThread(LeaderboardEnum.BOUNTY_UPVOTES, this, repQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        repThread3.init();
        threadIds.add(repThread3.getId());
        LeaderboardReputationThread repThread4 = new LeaderboardReputationThread(LeaderboardEnum.BOUNTY_DOWNVOTES, this, repQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        repThread4.init();
        threadIds.add(repThread4.getId());
        LeaderboardReputationThread repThread5 = new LeaderboardReputationThread(LeaderboardEnum.COMMENT_UPVOTES, this, repQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        repThread5.init();
        threadIds.add(repThread5.getId());
        LeaderboardReputationThread repThread6 = new LeaderboardReputationThread(LeaderboardEnum.COMMENT_DOWNVOTES, this, repQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        repThread6.init();
        threadIds.add(repThread6.getId());
        LeaderboardReputationThread repThread7 = new LeaderboardReputationThread(LeaderboardEnum.REPLYS_TO_COMMENTS, this, repQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        repThread7.init();
        threadIds.add(repThread7.getId());
        LeaderboardReputationThread repThread8 = new LeaderboardReputationThread(LeaderboardEnum.CLAIMS_ON_BOUNTIES, this, repQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        repThread8.init();
        threadIds.add(repThread8.getId());
        LeaderboardUserMetricThread userThread = new LeaderboardUserMetricThread(LeaderboardEnum.OPEN_BOUNTIES, this, userMetricQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        userThread.init();
        threadIds.add(userThread.getId());
        LeaderboardUserMetricThread userThread2 = new LeaderboardUserMetricThread(LeaderboardEnum.CLAIMED_BOUNTIES, this, userMetricQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        userThread2.init();
        threadIds.add(userThread2.getId());
        LeaderboardUserMetricThread userThread3 = new LeaderboardUserMetricThread(LeaderboardEnum.EXPIRED_BOUNTIES, this, userMetricQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        userThread3.init();
        threadIds.add(userThread3.getId());
        LeaderboardUserMetricThread userThread4 = new LeaderboardUserMetricThread(LeaderboardEnum.STALE_BOUNTIES, this, userMetricQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        userThread4.init();
        threadIds.add(userThread4.getId());
        LeaderboardUserMetricThread userThread5 = new LeaderboardUserMetricThread(LeaderboardEnum.TOTAL_BOUNTIES, this, userMetricQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        userThread5.init();
        threadIds.add(userThread5.getId());
        LeaderboardUserMetricThread userThread6 = new LeaderboardUserMetricThread(LeaderboardEnum.OPEN_CLAIMS, this, userMetricQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        userThread6.init();
        threadIds.add(userThread6.getId());
        LeaderboardUserMetricThread userThread7 = new LeaderboardUserMetricThread(LeaderboardEnum.ACCEPTED_CLAIMS, this, userMetricQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        userThread7.init();
        threadIds.add(userThread7.getId());
        LeaderboardUserMetricThread userThread8 = new LeaderboardUserMetricThread(LeaderboardEnum.REJ_CLAIMS, this, userMetricQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        userThread8.init();
        threadIds.add(userThread8.getId());
        LeaderboardUserMetricThread userThread9 = new LeaderboardUserMetricThread(LeaderboardEnum.TOTAL_CLAIMS, this, userMetricQueryHandler, leaderQueryHandler, ymd, yymd, PAGE_SIZE);
        userThread9.init();
        threadIds.add(userThread9.getId());
        
        int sleepCtr = 0;
        while (!threadIds.isEmpty()) {
            try {
                Thread.sleep(500);
                if (++sleepCtr > 60) {
                    logger.info("Waiting for [" + threadIds.size() + "] threads to finish");
                }
            } catch (Exception e) {
            }
        }
        // save anything that hasn't been saved yet
        bulkSave();
        SqsData data = new SqsData(new Leaderboard());
        data.setSendToAll(Boolean.TRUE);
        sqsService.addToQueue(data);
        sqsService.destroy();
        Date end = new Date();
        Long timeSec = (end.getTime() - start.getTime()) / 1000l;
        twitterHandler.sendLeaderboardTweet();
        logger.info("FInished! Processed in [" + timeSec + "] seconds");
        
        return "Ok";
    }

    /**
     *
     * @param lb
     */
    public void processLeaderboard(Leaderboard lb) {
        synchronized (LOCK) {
            lbs.add(lb);
        }
    }

    /**
     * As each thread finishes, it'll call this method
     *
     * @param id
     */
    public void finishedTimeThread(String id) {
        this.threadIds.remove(id);
    }

    /**
     *
     * @return
     */
    public int getThreadIdsSize() {
        return this.threadIds.size();
    }

    /**
     * Bulk save what's been collected
     */
    public synchronized void bulkSave() {
        if (!lbs.isEmpty()) {
            synchronized (LOCK) {
                BulkRequest.Builder brLb = new BulkRequest.Builder();
                for (Leaderboard lb : lbs) {
                    ObjectNode node = mapper.convertValue(lb, ObjectNode.class);
                    node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Leaderboard");
                    brLb.operations(op -> op
                            .index(idx -> idx
                            .index(Leaderboard.INDEX_NAME)
                            .id(lb.getId())
                            .document(node)
                            )
                    );
                }
                try {
                    BulkResponse result = esClient.bulk(brLb.build());
                    if (result.errors()) {
                        logger.error("Bulk save errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while updating leaderboards! [" + ex.getMessage() + "]", ex);
                }
                lbs.clear();
            }
        }
    }
}
