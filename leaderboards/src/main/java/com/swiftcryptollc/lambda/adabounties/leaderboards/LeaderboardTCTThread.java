package com.swiftcryptollc.lambda.adabounties.leaderboards;

import com.swiftcryptollc.commons.adabounties.data.LeaderboardEnum;
import com.swiftcryptollc.commons.adabounties.data.Timeframe;
import com.swiftcryptollc.commons.adabounties.es.data.Leaderboard;
import com.swiftcryptollc.commons.adabounties.es.data.LeaderboardEntry;
import com.swiftcryptollc.commons.adabounties.es.data.TokenClaimTotal;
import com.swiftcryptollc.commons.adabounties.es.query.LeaderboardQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.TCTQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.data.SyncArrayDeque;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class LeaderboardTCTThread extends AbstractLeaderboardThread {

    private final TCTQueryHandler tctQueryHandler;
    protected final static Logger logger = LoggerFactory.getLogger(LeaderboardTCTThread.class);

    public LeaderboardTCTThread(LeaderboardEnum type, LeaderboardService leaderboardService,
            TCTQueryHandler tctQueryHandler, LeaderboardQueryHandler leaderQueryHandler,
            String ymd, String yymd, String token, Integer pageSize) {
        super(type, leaderboardService, leaderQueryHandler, ymd, yymd, token, pageSize);
        this.tctQueryHandler = tctQueryHandler;
    }

    /**
     *
     */
    @Override
    public void run() {
        try {
            logger.info("Collecting TCT leaders");
            Leaderboard leaderboard = initToday();
            PageRequest pageable = PageRequest.of(1, pageSize);
            SyncArrayDeque<TokenClaimTotal> tctSet = tctQueryHandler.findTokenClaimTotals(null, token, Timeframe.DAYS_ALL.label, pageable);
            int rank = 1;
            Double preValue = Double.MIN_VALUE;
            Integer preRank = 1;
            LeaderboardEntry lbe;
            TokenClaimTotal tct = null;
            while ((tct = tctSet.pollFirst()) != null) {
                Double value = tct.getAmount();
                lbe = processEntry(leaderboard, tct.getAccountId(), preRank, rank, preValue, value);
                preValue = lbe.getValue();
                preRank = lbe.getRank();
                ++rank;
            }
            leaderboardService.processLeaderboard(leaderboard);

        } catch (Exception ex) {
            logger.error("Exception occured while processing TCT leaderboard [" + ex.getMessage() + "]");
        }
        leaderboardService.finishedTimeThread(id);
    }
}
