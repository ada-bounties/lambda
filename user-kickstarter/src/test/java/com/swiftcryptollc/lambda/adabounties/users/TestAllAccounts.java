package com.swiftcryptollc.lambda.adabounties.users;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.data.Account;
import com.swiftcryptollc.commons.adabounties.es.query.AccountQueryHandler;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
@ConfigurationProperties(ignoreUnknownFields = true, prefix = "swiftcryptollc.adabounties.es")
public class TestAllAccounts {

    protected static AccountQueryHandler accountQueryHandler;
    private static LambdaElasticsearchClientConfig esClientConfig;
    private static ElasticsearchClient client;

    static {
        esClientConfig = new LambdaElasticsearchClientConfig();
        client = esClientConfig.elasticSearchClient();
        accountQueryHandler = new AccountQueryHandler();
        accountQueryHandler.setClient(client);
    }

    @Test
    public void testFreeSearch() {
//        try {
//
//            int i = 1;
//            Set<AccountIdMap> accounts = new HashSet<>();
//
//            do {
//                PageRequest pageable = PageRequest.of(i++, 20);
//                accounts = queryHandler.findAllAccounts(pageable);
//                System.out.println("Found [" + accounts.size() + "] AccountIdMaps");
//            } while (!accounts.isEmpty());
//
//        } catch (Exception ex) {
//            System.out.println("Exception! [" + ex.getMessage() + "]");
//            ex.printStackTrace();
//        }

        PageRequest pageable = PageRequest.of(1, 200);
        Set<Account> accounts = accountQueryHandler.findAllAccounts(pageable);
        System.out.println("Found [" + accounts.size() + "] Accounts");
    }
}
