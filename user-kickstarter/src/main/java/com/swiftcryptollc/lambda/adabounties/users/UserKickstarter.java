package com.swiftcryptollc.lambda.adabounties.users;

import ch.qos.logback.classic.Level;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.swiftcryptollc.commons.adabounties.aws.data.CardanoNetwork;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.data.Account;
import com.swiftcryptollc.commons.adabounties.es.query.AccountQueryHandler;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

/**
 * Will get all of the user accounts and write chunks of them to different S3
 * files.
 *
 * Each of the S3 files will get picked up by a UserMetrics Lambda function and
 * run the calculation for each user listed in the S3 file (divide and conquer).
 *
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class UserKickstarter implements RequestHandler<Object, String> {

    private final static String PREVIEW_DEST_NOTIFICATIONS_BUCKET = "adabounties-preview-notifications";
    private final static String MAINNET_DEST_NOTIFICATIONS_BUCKET = "adabounties-notifications";
    private static String DEST_NOTIFICATIONS_BUCKET;
    private final static String PREVIEW_DEST_METRICS_BUCKET = "adabounties-preview-metrics";
    private final static String MAINNET_DEST_METRICS_BUCKET = "adabounties-metrics";
    private static String DEST_METRICS_BUCKET;

    private final static LambdaElasticsearchClientConfig esClientConfig;
    private final static ElasticsearchClient esClient;
    private final static AccountQueryHandler queryHandler;
    protected final static Logger logger = LoggerFactory.getLogger(UserKickstarter.class);

    static {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        esClientConfig = new LambdaElasticsearchClientConfig();
        esClient = esClientConfig.elasticSearchClient();
        queryHandler = new AccountQueryHandler(esClient);

        String network = System.getenv("cardano_network");
        if (CardanoNetwork.MAINNET.label.equalsIgnoreCase(network)) {
            DEST_NOTIFICATIONS_BUCKET = MAINNET_DEST_NOTIFICATIONS_BUCKET;
            DEST_METRICS_BUCKET = MAINNET_DEST_METRICS_BUCKET;
        } else {
            DEST_NOTIFICATIONS_BUCKET = PREVIEW_DEST_NOTIFICATIONS_BUCKET;
            DEST_METRICS_BUCKET = PREVIEW_DEST_METRICS_BUCKET;
        }
    }

    /**
     * Default Constructor
     */
    public UserKickstarter() {
    }

    /**
     * We aren't using the event information since this is a simple time
     * scheduled "cron" job
     *
     * Max Lambda execution time is 15 minutes.
     *
     * @param eventObject
     * @param context
     * @return
     */
    @Override
    public String handleRequest(Object eventObject, Context context) {
        Date start = new Date();

        int i = 1;
        Set<Account> accounts = new HashSet<>();

        do {
            PageRequest pageable = PageRequest.of(i++, 250);
            accounts = queryHandler.findAllAccounts(pageable);
            if (!accounts.isEmpty()) {
                try {
                    boolean addedNotifications = false;
                    logger.info("Processing[" + accounts.size() + "] accounts");
                    File notificationsFile = new File(getNewFileName());
                    BufferedWriter bwNotifications = new BufferedWriter(new FileWriter(notificationsFile));
                    File metricsFile = new File(getNewFileName());
                    BufferedWriter bwMetrics = new BufferedWriter(new FileWriter(metricsFile));
                    for (Account account : accounts) {
                        if ((account.getEmail() != null)
                                && (account.getNotify())) {
                            addedNotifications = true;
                            bwNotifications.write(account.getId().concat("=").concat(account.getEmail()).concat("=").concat(account.getChallenge()));
                            bwNotifications.newLine();
                        }
                        bwMetrics.write(account.getId());
                        bwMetrics.newLine();

                    }
                    bwNotifications.close();
                    bwMetrics.close();
                    if (addedNotifications) {
                        uploadFile(notificationsFile, DEST_NOTIFICATIONS_BUCKET);
                    } else {
                        try {
                            notificationsFile.delete();
                        } catch (Exception e) {
                        }
                    }
                    uploadFile(metricsFile, DEST_METRICS_BUCKET);
                } catch (IOException ioex) {
                    logger.error("Exception occured! [" + ioex.getMessage() + "]");
                    break;
                }
            }
        } while (!accounts.isEmpty());

        Date end = new Date();
        Long timeSec = (end.getTime() - start.getTime()) / 1000l;
        logger.info("Finished in [" + timeSec + "] seconds");

        return "Ok";
    }

    /**
     * Generate a new file name
     *
     * @return
     */
    public String getNewFileName() {
        String fileName = "/tmp/" + UUID.randomUUID().toString().concat(".csv");
        return fileName;
    }

    /**
     * Upload the new files for loading
     *
     * @param fileToUpload
     * @param bucket
     */
    public void uploadFile(File fileToUpload, String bucket) {
        final AmazonS3 s3 = AmazonS3ClientBuilder.standard().withRegion(Regions.US_EAST_2).build();
        try {
            s3.putObject(bucket, fileToUpload.getName(), fileToUpload);
            fileToUpload.delete();
        } catch (AmazonServiceException ex) {
            System.err.println("Exception occured while trying to upload the new file [" + ex.getMessage() + "]");
        }
    }
}
