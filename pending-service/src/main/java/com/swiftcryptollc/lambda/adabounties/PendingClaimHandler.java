package com.swiftcryptollc.lambda.adabounties;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import com.bloxbean.cardano.client.backend.api.BackendService;
import com.bloxbean.cardano.client.backend.blockfrost.common.Constants;
import com.bloxbean.cardano.client.backend.blockfrost.service.BFBackendService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import com.swiftcryptollc.commons.adabounties.aws.sqs.SqsService;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.data.AfterActionData;
import com.swiftcryptollc.commons.adabounties.data.BountyStatus;
import com.swiftcryptollc.commons.adabounties.data.ClaimStatus;
import com.swiftcryptollc.commons.adabounties.es.data.Accounting;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.data.Claim;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.ClaimQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.cardano.CardanoTxUtils;
import com.swiftcryptollc.commons.adabounties.utilities.data.ActionType;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class PendingClaimHandler implements Runnable {

    protected PendingService pendingService;
    protected String bfUrl;
    protected String bfTxCheckUrl;
    protected String bfProjectId;
    protected String network;
    protected BackendService backendService;
    protected String adaPriceUrl;
    protected Long cutoffMs;
    private final BountyQueryHandler btyQueryHandler;
    private final ClaimQueryHandler clmQueryHandler;
    private final SqsService sqsService;
    private final ElasticsearchClient esClient;

    protected final static Logger logger = LoggerFactory.getLogger(PendingClaimHandler.class);

    public PendingClaimHandler(PendingService pendingService, ElasticsearchClient esClient, BountyQueryHandler btyQueryHandler, ClaimQueryHandler clmQueryHandler, SqsService sqsService, Long cutoffMs, String adaPriceUrl) {
        this.pendingService = pendingService;
        this.esClient = esClient;
        this.btyQueryHandler = btyQueryHandler;
        this.clmQueryHandler = clmQueryHandler;
        this.sqsService = sqsService;
        this.adaPriceUrl = adaPriceUrl;
        this.cutoffMs = cutoffMs;
    }

    private void setupBFProject(Map<String, String> secretMap) {
        try {
            if (secretMap.containsKey("cardano_bfProjectId")) {
                bfProjectId = secretMap.get("cardano_bfProjectId");
            } else {
                logger.warn("cardano_bfProjectId not set");
            }
        } catch (Exception ex) {
            logger.error("Could not read the cardano_bfProjectId from the SecretsManager [" + ex.getMessage() + "]");
        }
        try {
            if (secretMap.containsKey("cardano_network")) {
                network = secretMap.get("cardano_network");
            } else {
                logger.warn("cardano_network not set");
            }
        } catch (Exception ex) {
            logger.error("Could not read the cardano_network from the SecretsManager [" + ex.getMessage() + "]");
        }

        if (network.equals("mainnet")) {
            bfUrl = Constants.BLOCKFROST_MAINNET_URL;
            backendService = new BFBackendService(
                    Constants.BLOCKFROST_MAINNET_URL, bfProjectId);
            bfTxCheckUrl = Constants.BLOCKFROST_MAINNET_URL.concat("txs/");
        } else {
            bfUrl = Constants.BLOCKFROST_PREVIEW_URL;
            backendService = new BFBackendService(
                    Constants.BLOCKFROST_PREVIEW_URL, bfProjectId);
            bfTxCheckUrl = Constants.BLOCKFROST_PREVIEW_URL.concat("txs/");
        }
    }

    @Override
    public void run() {
        try {
            logger.info("Checking for pending claims");
            ObjectMapper mapper = new ObjectMapper();

            Map<String, String> secretMap = SecretsManager.getSecretMap();
            setupBFProject(secretMap);

            int index = 1;
            Set<Claim> claimList = new HashSet<>();

            CardanoTxUtils txUtils = new CardanoTxUtils(bfUrl, bfProjectId, bfTxCheckUrl);
            BulkRequest.Builder brAccepted = new BulkRequest.Builder();
            BulkRequest.Builder brOpen = new BulkRequest.Builder();
            BulkRequest.Builder btyOpen = new BulkRequest.Builder();
            BulkRequest.Builder btyClaimed = new BulkRequest.Builder();
            BulkRequest.Builder accountingDelete = new BulkRequest.Builder();
            Set<String> openBountyIds = new HashSet<>();
            Set<String> claimedBountyIds = new HashSet<>();
            int numClaimed = 0;
            int numOpen = 0;
            int numDelete = 0;
            Long now = new Date().getTime();

            do {
                PageRequest pageable = PageRequest.of(index, 100);
                claimList = clmQueryHandler.findPendingClaims(cutoffMs, pageable);
                logger.info("Processing [" + claimList.size() + "] pending claims");
                for (Claim claim : claimList) {
                    ++numDelete;
                    String txHash = claim.getTxHash();
                    Boolean found = txUtils.checkTx(txHash);
                    try {
                        if (!found) {
                            logger.info("Not found [" + claim.getId() + "] [" + txHash + "] - Update to OPEN");
                            ++numOpen;
                            claim.setTxHash(null);
                            claim.setUpdatedMs(now);
                            openBountyIds.add(claim.getBountyId());
                            claim.setStatus(ClaimStatus.OPEN.label);
                            ObjectNode node = mapper.convertValue(claim, ObjectNode.class);
                            node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Claim");
                            brOpen.operations(op -> op
                                    .index(idx -> idx
                                    .index(Claim.INDEX_NAME)
                                    .id(claim.getId())
                                    .document(node)
                                    )
                            );
                            accountingDelete.operations(op -> op
                                    .delete(idx -> idx
                                    .index(Accounting.INDEX_NAME)
                                    .id(txHash)
                                    )
                            );
                            sqsService.addToQueue(new SqsData(new AfterActionData(ActionType.CLAIM_UPDATE, claim)));

                        } else {
                            logger.info("Found [" + claim.getId() + "] [" + txHash + "] - Update to ACCEPTED");
                            ++numClaimed;
                            claimedBountyIds.add(claim.getBountyId());
                            claim.setStatus(ClaimStatus.ACCEPTED.label);
                            ObjectNode node = mapper.convertValue(claim, ObjectNode.class);
                            node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Claim");
                            brAccepted.operations(op -> op
                                    .index(idx -> idx
                                    .index(Claim.INDEX_NAME)
                                    .id(claim.getId())
                                    .document(node)
                                    )
                            );

                            sqsService.addToQueue(new SqsData(new AfterActionData(ActionType.CLAIM_ACCEPT, claim)));
                        }
                    } catch (Exception e) {
                        logger.error("Exception occured while process [" + claim.getId() + "] [" + e.getMessage() + "]", e);
                    }
                }

                ++index;
            } while (!claimList.isEmpty());

            if (numOpen > 0) {
                try {
                    logger.info("Setting [" + numOpen + "] claims to Open");
                    BulkResponse result = esClient.bulk(brOpen.build());
                    if (result.errors()) {
                        logger.error("Bulk save errors encountered for open claims");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }

                    Set<Bounty> bounties = btyQueryHandler.findAllById(openBountyIds);
                    logger.info("Updating [" + bounties.size() + "] bounties back to Open");
                    for (Bounty bty : bounties) {
                        bty.setUpdatedMs(now);
                        if (bty.getExpireMs() > now) {
                            bty.setStatus(BountyStatus.OPEN.label);
                        } else {
                            bty.setStatus(BountyStatus.EXPIRED.label);
                        }
                        ObjectNode node = mapper.convertValue(bty, ObjectNode.class);
                        node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Bounty");
                        btyOpen.operations(op -> op
                                .index(idx -> idx
                                .index(Bounty.INDEX_NAME)
                                .id(bty.getId())
                                .document(node)
                                )
                        );
                        sqsService.addToQueue(new SqsData(new AfterActionData(ActionType.BOUNTY_UPDATE, bty)));
                    }
                    BulkResponse result2 = esClient.bulk(btyOpen.build());
                    if (result2.errors()) {
                        logger.error("Bulk save errors encountered for opened bounties");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }

                } catch (Exception ex) {
                    logger.error("Exception occured while updating the bounties from pending to open! [" + ex.getMessage() + "]", ex);
                }

            }
            if (numClaimed > 0) {
                try {
                    logger.info("Setting [" + numClaimed + "] claims to Accepted");
                    BulkResponse result = esClient.bulk(brAccepted.build());
                    if (result.errors()) {
                        logger.error("Bulk save errors encountered for accepted claims");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }

                    Set<Bounty> bounties = btyQueryHandler.findAllById(claimedBountyIds);
                    logger.info("Updating [" + bounties.size() + "] bounties to Claimed");
                    for (Bounty bty : bounties) {
                        bty.setUpdatedMs(now);
                        bty.setStatus(BountyStatus.CLAIMED.label);
                        ObjectNode node = mapper.convertValue(bty, ObjectNode.class);
                        node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Bounty");
                        btyClaimed.operations(op -> op
                                .index(idx -> idx
                                .index(Bounty.INDEX_NAME)
                                .id(bty.getId())
                                .document(node)
                                )
                        );
                        sqsService.addToQueue(new SqsData(new AfterActionData(ActionType.BOUNTY_CLAIMED, bty)));
                    }
                    BulkResponse result2 = esClient.bulk(btyClaimed.build());
                    if (result2.errors()) {
                        logger.error("Bulk save errors encountered for claimed bounties");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while updating the claims/bounties from pending to accepted/claimed! [" + ex.getMessage() + "]", ex);
                }
            }
            if (numDelete > 0) {
                try {
                    logger.info("Deleting invalid Accounting records..");
                    BulkResponse result = esClient.bulk(accountingDelete.build());
                    if (result.errors()) {
                        logger.error("Bulk delete errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while deleting invalid accounting records! [" + ex.getMessage() + "]", ex);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception during bounty pending handling [" + ex.getMessage() + "]", ex);
        }
        pendingService.finishedClaims();
    }
}
