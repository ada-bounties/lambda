package com.swiftcryptollc.lambda.adabounties;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import com.bloxbean.cardano.client.backend.api.BackendService;
import com.bloxbean.cardano.client.backend.blockfrost.common.Constants;
import com.bloxbean.cardano.client.backend.blockfrost.service.BFBackendService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import com.swiftcryptollc.commons.adabounties.aws.sqs.SqsService;
import com.swiftcryptollc.commons.adabounties.aws.sqs.data.SqsData;
import com.swiftcryptollc.commons.adabounties.data.AfterActionData;
import com.swiftcryptollc.commons.adabounties.data.BountyStatus;
import com.swiftcryptollc.commons.adabounties.es.data.Accounting;
import com.swiftcryptollc.commons.adabounties.es.data.Bounty;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.utilities.cardano.CardanoTxUtils;
import com.swiftcryptollc.commons.adabounties.utilities.data.ActionType;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;

/**
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public class PendingBountyHandler implements Runnable {

    protected PendingService pendingService;
    protected String bfUrl;
    protected String bfTxCheckUrl;
    protected String bfProjectId;
    protected String network;
    protected BackendService backendService;
    private final ElasticsearchClient esClient;
    protected String adaPriceUrl;
    protected Long cutoffMs;
    private final BountyQueryHandler btyQueryHandler;
    private final SqsService sqsService;
    protected final static Logger logger = LoggerFactory.getLogger(PendingBountyHandler.class);

    public PendingBountyHandler(PendingService pendingService, ElasticsearchClient esClient, BountyQueryHandler btyQueryHandler, SqsService sqsService, Long cutoffMs, String adaPriceUrl) {
        this.pendingService = pendingService;
        this.esClient = esClient;
        this.btyQueryHandler = btyQueryHandler;
        this.sqsService = sqsService;
        this.adaPriceUrl = adaPriceUrl;
        this.cutoffMs = cutoffMs;
    }

    /**
     *
     * @param secretMap
     */
    private void setupBFProject(Map<String, String> secretMap) {
        try {
            if (secretMap.containsKey("cardano_bfProjectId")) {
                bfProjectId = secretMap.get("cardano_bfProjectId");
            } else {
                logger.warn("cardano_bfProjectId not set");
            }
        } catch (Exception ex) {
            logger.error("Could not read the cardano_bfProjectId from the SecretsManager [" + ex.getMessage() + "]");
        }
        try {
            if (secretMap.containsKey("cardano_network")) {
                network = secretMap.get("cardano_network");
            } else {
                logger.warn("cardano_network not set");
            }
        } catch (Exception ex) {
            logger.error("Could not read the cardano_network from the SecretsManager [" + ex.getMessage() + "]");
        }

        if (network.equals("mainnet")) {
            bfUrl = Constants.BLOCKFROST_MAINNET_URL;
            backendService = new BFBackendService(
                    Constants.BLOCKFROST_MAINNET_URL, bfProjectId);
            bfTxCheckUrl = Constants.BLOCKFROST_MAINNET_URL.concat("txs/");
        } else {
            bfUrl = Constants.BLOCKFROST_PREVIEW_URL;
            backendService = new BFBackendService(
                    Constants.BLOCKFROST_PREVIEW_URL, bfProjectId);
            bfTxCheckUrl = Constants.BLOCKFROST_PREVIEW_URL.concat("txs/");
        }
    }

    @Override
    public void run() {
        try {
            logger.info("Checking for pending bounties");

            ObjectMapper mapper = new ObjectMapper();

            Map<String, String> secretMap = SecretsManager.getSecretMap();
            setupBFProject(secretMap);

            int index = 1;
            Set<Bounty> bountyList = new HashSet<>();

            CardanoTxUtils txUtils = new CardanoTxUtils(bfUrl, bfProjectId, bfTxCheckUrl);
            BulkRequest.Builder brDraft = new BulkRequest.Builder();
            BulkRequest.Builder brOpen = new BulkRequest.Builder();
            BulkRequest.Builder accountingDelete = new BulkRequest.Builder();
            int numOpen = 0;
            int numDraft = 0;
            int numDelete = 0;
            Long now = new Date().getTime();

            do {
                PageRequest pageable = PageRequest.of(index, 100);
                bountyList = btyQueryHandler.findPendingBounties(cutoffMs, pageable);
                logger.info("Processing [" + bountyList.size() + "] pending bounties");

                for (Bounty bty : bountyList) {
                    ++numDelete;
                    String txHash = bty.getTxHash();
                    Boolean found = txUtils.checkTx(txHash);
                    try {
                        if (found) {
                            logger.info("Found [" + bty.getId() + "] [" + txHash + "] - Update to OPEN");
                            ++numOpen;
                            bty.setUpdatedMs(now);
                            bty.setStatus(BountyStatus.OPEN.label);
                            ObjectNode node = mapper.convertValue(bty, ObjectNode.class);
                            node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Bounty");
                            brOpen.operations(op -> op
                                    .index(idx -> idx
                                    .index(Bounty.INDEX_NAME)
                                    .id(bty.getId())
                                    .document(node)
                                    )
                            );
                            sqsService.addToQueue(new SqsData(new AfterActionData(ActionType.BOUNTY_NEW, bty)));

                        } else {
                            logger.info("Did not find [" + bty.getId() + "] [" + txHash + "] - Update to Draft");
                            ++numDraft;
                            bty.setTxHash(null);
                            bty.setStatus(BountyStatus.DRAFT.label);
                            ObjectNode node = mapper.convertValue(bty, ObjectNode.class);
                            node.put("_class", "com.swiftcryptollc.commons.adabounties.es.data.Bounty");
                            brDraft.operations(op -> op
                                    .index(idx -> idx
                                    .index(Bounty.INDEX_NAME)
                                    .id(bty.getId())
                                    .document(node)
                                    )
                            );
                            accountingDelete.operations(op -> op
                                    .delete(idx -> idx
                                    .index(Accounting.INDEX_NAME)
                                    .id(txHash)
                                    )
                            );
                            sqsService.addToQueue(new SqsData(new AfterActionData(ActionType.BOUNTY_UPDATE, bty)));
                        }

                    } catch (Exception e) {
                        logger.error("Exception occured while process [" + bty.getId() + "] [" + e.getMessage() + "]", e);
                    }
                }

                ++index;
            } while (!bountyList.isEmpty());

            if (numOpen > 0) {
                try {
                    logger.info("Setting [" + numOpen + "] bounties to Open");
                    BulkResponse result = esClient.bulk(brOpen.build());
                    if (result.errors()) {
                        logger.error("Bulk save errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while updating the bounties from pending to open! [" + ex.getMessage() + "]", ex);
                }
            }
            if (numDraft > 0) {
                try {
                    logger.info("Setting [" + numDraft + "] bounties back to Draft");
                    BulkResponse result = esClient.bulk(brDraft.build());
                    if (result.errors()) {
                        logger.error("Bulk save errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while updating the bounties from pending to draft! [" + ex.getMessage() + "]", ex);
                }
            }
            if (numDelete > 0) {
                try {
                    logger.info("Deleting invalid Accounting records..");
                    BulkResponse result = esClient.bulk(accountingDelete.build());
                    if (result.errors()) {
                        logger.error("Bulk delete errors encountered");
                        for (BulkResponseItem item : result.items()) {
                            if (item.error() != null) {
                                logger.error(item.error().reason());
                            }
                        }
                    }
                } catch (Exception ex) {
                    logger.error("Exception occured while deleting invalid accounting records! [" + ex.getMessage() + "]", ex);
                }
            }
        } catch (Exception ex) {
            logger.error("Exception during bounty pending handling [" + ex.getMessage() + "]", ex);
        }
        pendingService.finishedBounties();
    }
}
