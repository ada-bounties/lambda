package com.swiftcryptollc.lambda.adabounties;

import ch.qos.logback.classic.Level;
import co.elastic.clients.elasticsearch.ElasticsearchClient;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.swiftcryptollc.commons.adabounties.aws.secrets.SecretsManager;
import com.swiftcryptollc.commons.adabounties.aws.sqs.SqsService;
import com.swiftcryptollc.commons.adabounties.es.LambdaElasticsearchClientConfig;
import com.swiftcryptollc.commons.adabounties.es.query.BountyQueryHandler;
import com.swiftcryptollc.commons.adabounties.es.query.ClaimQueryHandler;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Run as a "cron job" that is triggered from Amazon EventBridge Service to
 * update bounties
 *
 *
 * @author Steven K Fisher <fisherstevenk@gmail.com>
 */
public final class PendingService implements RequestHandler<Object, String> {

    private final static SqsService sqsService;
    private final static LambdaElasticsearchClientConfig esClientConfig;
    private final static ElasticsearchClient esClient;
    private final static BountyQueryHandler btyQueryHandler;
    private final static ClaimQueryHandler clmQueryHandler;
    private final static Long THIRTYMINUTES = 1800000L;
    private static Boolean finishedBounties = false;
    private static Boolean finishedClaims = false;
    protected static String adaPriceUrl;
    protected Long timeOverrideMs = null;
    protected final static Logger logger = LoggerFactory.getLogger(PendingService.class);

    static {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(org.slf4j.Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.INFO);
        esClientConfig = new LambdaElasticsearchClientConfig();
        esClient = esClientConfig.elasticSearchClient();
        btyQueryHandler = new BountyQueryHandler();
        btyQueryHandler.setClient(esClient);
        clmQueryHandler = new ClaimQueryHandler();
        clmQueryHandler.setClient(esClient);
        sqsService = new SqsService(true);
        adaPriceUrl = SecretsManager.getSecretMap().get("adaPriceUrl");
    }

    /**
     * Default Constructor
     */
    public PendingService() {
    }

    /**
     * We aren't using the event information since this is a simple time
     * scheduled "cron" job
     *
     * Max Lambda execution time is 15 minutes.
     *
     * @param eventObject
     * @param context
     * @return
     */
    @Override
    public String handleRequest(Object eventObject, Context context) {
        Long now = new Date().getTime();
        logger.info("Searching for pending bounties and claims");
        Long subtract = timeOverrideMs;
        if (subtract == null) {
            subtract = THIRTYMINUTES;
        }
        Long cutoffMs = now - subtract;
        logger.info("Check claims");
        PendingClaimHandler clmHandler = new PendingClaimHandler(this, esClient, btyQueryHandler, clmQueryHandler, sqsService, cutoffMs, adaPriceUrl);
        new Thread(clmHandler).start();
        while (!finishedClaims) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
        }
        logger.info("Check bounties");
        PendingBountyHandler btyHandler = new PendingBountyHandler(this, esClient, btyQueryHandler, sqsService, cutoffMs, adaPriceUrl);
        new Thread(btyHandler).start();
        while (!finishedBounties) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
            }
        }
        Date end = new Date();
        Long timeSec = (end.getTime() - now) / 1000l;
        logger.info("Processed pending bounties/claims in [" + timeSec + "] seconds");
        sqsService.destroy();
        return "Ok";
    }

    public void finishedBounties() {
        logger.info("Finished processing bounties");
        finishedBounties = true;
    }

    public void finishedClaims() {
        logger.info("Finished processing claims");
        finishedClaims = true;
    }

    /**
     * @param timeOverrideMs the timeOverrideMs to set
     */
    public void setTimeOverrideMs(Long timeOverrideMs) {
        this.timeOverrideMs = timeOverrideMs;
    }
}
